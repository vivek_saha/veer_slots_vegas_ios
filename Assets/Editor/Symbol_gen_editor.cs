﻿using UnityEngine;
using System.Collections;
using UnityEditor;

[CustomEditor(typeof(symbol_generatoion))]
public class LevelScriptEditor : Editor
{
    public override void OnInspectorGUI()
    {
        symbol_generatoion  myTarget = (symbol_generatoion)target;

        //myTarget.experience = EditorGUILayout.IntField("Experience", myTarget.experience);

//EditorGUILayout.BeginToggleGroup("createfolder", true);
            {
            // EditorGUILayout.LabelField("FolderName", myTarget.folder_name.ToString());
            //EditorGUILayout.TextField("Name : ", myTarget.folder_name);
        }
        //EditorGUI.TextField(Rect.zero, myTarget.folder_name);
        var style = new GUIStyle(GUI.skin.button);
        style.active.textColor = Color.red;
        if (GUILayout.Button("create folder",style))
        {
            myTarget.create_theme_folder();
        }
        style.active.textColor = Color.blue;
        if (GUILayout.Button("Make_symbols_prefabs", style))
        {
            myTarget.Make_symbols_prefabs();
        }
        style.active.textColor = Color.green;
        if (GUILayout.Button("Add to Slot", style))
        {
            myTarget.Addtoslot();
        }
        style.active.textColor = Color.magenta;
        if (GUILayout.Button("Create_lvl_prefab", style))
        {
            myTarget.Create_lvl_prefab();
        }
        //if (GUILayout.Button("Change_payline_color"))
        //{
        //    myTarget.Change_payline_color();
        //}
        //if (GUILayout.Button("Adding_Betperline"))
        //{
        //    myTarget.Adding_Betperline();
        //}

        DrawDefaultInspector();

        // PrefabUtility.CreatePrefab(themefolderpath, go);
    }

    //public void create_prefab()
    //{
    //    symbol_generatoion myTarget = (symbol_generatoion)target;
       
    //}
}