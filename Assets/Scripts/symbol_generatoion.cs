﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;
using System.IO;
using aSlot;

public class symbol_generatoion : MonoBehaviour
{
#if UNITY_EDITOR

    [SerializeField]
    public Slot slot;

    //public GameObject sym1;

    private GameObject go, preffab;

    //public string themefolderpath=null,folder_name="1";
    public int Level = 1;
    [HideInInspector]
    public int temp_num = 0;
    //public bool create_folder;
    [HideInInspector]
    public string[] Name;

    public bool square_winbox_bool = false;

    [HideInInspector]
    public SpriteRenderer BG, Reel_container;

    [HideInInspector]
    public GameObject circle_winbox, square_winbox;


    private void Awake()
    {



    }
    private void Start()
    {
        //foreach(SlotSymbolInfo a in slot.symbolInfo)
        //print(slot.symbolInfo[12].isWild);
    }
    public void Make_symbols_prefabs()
    {
        foreach (string a in Name)
        {
            make_symbol();
        }
        temp_num = 0;
    }
    public void Addtoslot()
    {
        slot.gameObject.transform.parent.gameObject.SetActive(true);
        //for (int i = 0; i < 4; i++)
        //{
        //    Adding_Reels();
        //}
        Adding_Betperline();
        foreach (string a in Name)
        {
            removing_symbols();
            Removing_symbols_set();
        }

        foreach (string a in Name)
        {
            //make_symbol();
            GameObject x = Resources.Load<GameObject>("Level_Prefabs/" + Level.ToString() + "/Icons_prefab/" + a);
            Symbol_adding(x);
            Add_symbols_set(a);
        }
        for (int i = 0; i < 12; i++)
        {
            if (i == 11)
            {
                slot.symbolInfo[i].isWild = true;
            }
            else
            {
                slot.symbolInfo[i].isWild = false;

            }
        }
        Change_BG_Reel_container();
        //Adding_Paylines();
        //slot.enabled = true;
        Change_payline_color();
        slot.gameObject.transform.parent.gameObject.SetActive(false);
    }
    public void make_symbol()
    {
        go = new GameObject("Symbol");
        go.AddComponent(typeof(SlotSymbol));
        go.AddComponent(typeof(SpriteRenderer));

        Sprite a;
        //select symbols form resources
        if (temp_num == 11)
        {
            a = Resources.Load<Sprite>("Level_Design/Wild_icons/" + Level.ToString());
        }
        else
        {
            a = Resources.Load<Sprite>("Level_Design/" + Level.ToString() + "/icons/" + Name[temp_num]);
        }
        go.GetComponent<SpriteRenderer>().sprite = a;
        go.GetComponent<SpriteRenderer>().sortingOrder = 1;
        go.GetComponent<SpriteRenderer>().maskInteraction = SpriteMaskInteraction.VisibleInsideMask;
        go.name = Name[temp_num];
        //print(a.name);
        //create_theme_folder("abc");


        preffab = PrefabUtility.CreatePrefab("Assets/Resources/Level_Prefabs/" + Level.ToString() + "/Icons_prefab/" + go.name + ".prefab", go);
        DestroyImmediate(go);
        temp_num++;
        //Symbol_adding();
        //PrefabUtility.SaveAsPrefabAsset(go, themefolderpath+"/");
        //if (themefolderpath == null)
        //{

        ////
        //}

        //


    }


    public void Adding_Betperline()
    {
        int s = slot.betsPerLine.Capacity;
        for (int i = 1; i < s; i++)
        {
            //print("slot.betsPerLine.Capacity" + slot.betsPerLine.Capacity);
            //print("slot.betsPerLine.Count" + slot.betsPerLine.Count);
            slot.betsPerLine.RemoveAt(slot.betsPerLine.Count - 1);
            slot.betsPerLine.Capacity = slot.betsPerLine.Capacity - 1;
        }
        long initial_value = 0;
        int divider = 0;
        for (int i = 1; i <= Level; i++)
        {
            if (Level <= 5)
            {
                initial_value += 200;
                divider = 200 / 4;
            }
            else if (Level >= 6 && Level <= 10)
            {
                initial_value += 500;
                divider = 500 / 5;
            }
            else if (Level >= 11 && Level <= 20)
            {
                initial_value += 1000;
                divider = 1000 / 5;
            }
            else if (Level >= 21 && Level <= 30)
            {
                initial_value += 5000;
                divider = 5000 / 5;
            }
            else if (Level >= 31 && Level <= 45)
            {
                initial_value += 10000;
                divider = 10000 / 5;
            }
            else if (Level >= 46 && Level <= 60)
            {
                initial_value += 50000;
                divider = 50000 / 5;
            }
            else if (Level >= 61 && Level <= 80)
            {
                initial_value += 100000;
                divider = (int)initial_value / 5;
            }
            else if (Level >= 81 && Level <= 90)
            {
                initial_value += 500000;
                divider = (int)initial_value / 5;
            }
            else if (Level >= 91 && Level <= 100)
            {
                initial_value += 1000000;
                divider = (int)initial_value / 5;
            }
        }
        for (int i = 0; i < 5; i++)
        {
            if (i > 0)
            {
                slot.betsPerLine.Capacity = slot.betsPerLine.Capacity + 1;
                slot.betsPerLine.Add(new BetsWrapper());
            }
            else
            {

            }
            slot.betsPerLine[slot.betsPerLine.Count - 1].value = initial_value + (i * divider);
            slot.betsPerLine[slot.betsPerLine.Count - 1].canBet = true;
        }
    }
    public void create_theme_folder(string s)
    {

        string guid = AssetDatabase.CreateFolder("Assets", s);
        //themefolderpath = AssetDatabase.GUIDToAssetPath(guid);
    }
    public void create_theme_folder()
    {


        AssetDatabase.CreateFolder("Assets/Resources/Level_Prefabs", Level.ToString());
        AssetDatabase.CreateFolder("Assets/Resources/Level_Prefabs/" + Level.ToString(), "Icons_prefab");
        //themefolderpath = AssetDatabase.GUIDToAssetPath(guid);
    }
    void Symbol_adding(GameObject AB)
    {
        slot.symbolPrefabs.Capacity = slot.symbolPrefabs.Capacity + 1;
        slot.symbolPrefabs.Add(AB);
        slot.winboxPrefabs.Capacity = slot.winboxPrefabs.Capacity + 1;
        if (AB.name == "Wild")
        {
            //slot.winboxPrefabs.Add(slot.winboxPrefabs[slot.winboxPrefabs.Count - 1]);
            slot.winboxPrefabs.Add(square_winbox);
        }
        else
        {
            if (square_winbox_bool)
            {
                slot.winboxPrefabs.Add(square_winbox);
            }
            else
            {
                slot.winboxPrefabs.Add(circle_winbox);
            }


        }


        slot.symbolBgPrefabs.Capacity = slot.symbolBgPrefabs.Capacity + 1;
        slot.symbolBgPrefabs.Add(null);
        if (AB.name == "Bonus")
        {
            slot.symbolInfo[slot.symbolPrefabs.Count - 1].clampPerReel = 1;
            slot.symbolInfo[slot.symbolPrefabs.Count - 1].clampTotal = 3;
        }
        slot.reelFrequencies.Capacity = slot.reelFrequencies.Capacity + 1;
        slot.reelFrequencies.Add(new FrequencyWrapper(slot.numberOfReels));

        slot.symbolFrequencies.Capacity = slot.symbolFrequencies.Capacity + 1;
        slot.symbolFrequencies.Add(1);

        slot.symbolInfo.Capacity = slot.symbolInfo.Capacity + 1;
        slot.symbolInfo.Add(new SlotSymbolInfo());



        //slot.symbolInfo[slot.symbolInfo.Capacity].isWild = true;
        //slot.symbolInfo.Add()
        //slot.symbolPrefabs.Add(sym1);
    }
    public void removing_symbols()
    {
        if (slot.symbolPrefabs.Count > 0)
        {
            slot.symbolPrefabs.RemoveAt(slot.symbolPrefabs.Count - 1);
            slot.symbolPrefabs.Capacity = slot.symbolPrefabs.Capacity - 1;
            slot.winboxPrefabs.RemoveAt(slot.winboxPrefabs.Count - 1);
            slot.winboxPrefabs.Capacity = slot.winboxPrefabs.Capacity - 1;

            while (slot.symbolBgPrefabs.Count > slot.symbolPrefabs.Count)
            {
                slot.symbolBgPrefabs.RemoveAt(slot.symbolBgPrefabs.Count - 1);
                slot.symbolBgPrefabs.Capacity = slot.symbolBgPrefabs.Capacity - 1;
            }

            while (slot.reelFrequencies.Count > slot.symbolPrefabs.Count)
            {
                slot.reelFrequencies.RemoveAt(slot.reelFrequencies.Count - 1);
                slot.reelFrequencies.Capacity = slot.reelFrequencies.Capacity - 1;
            }

            while (slot.symbolFrequencies.Count > slot.symbolPrefabs.Count)
            {
                slot.symbolFrequencies.RemoveAt(slot.symbolFrequencies.Count - 1);
                slot.symbolFrequencies.Capacity = slot.symbolFrequencies.Capacity - 1;
            }

            while (slot.symbolInfo.Count > slot.symbolInfo.Count)
            {
                slot.symbolInfo.RemoveAt(slot.symbolInfo.Count - 1);
                slot.symbolInfo.Capacity = slot.symbolInfo.Capacity - 1;
            }
        }
    }

    public void Add_symbols_set(string name)
    {
        slot.symbolSetNames.Capacity = slot.symbolSetNames.Capacity + 1;
        slot.symbolSetNames.Add(name);

        //slot.winningSymbolSets.Capacity = slot.winningSymbolSets.Capacity + 1;
        //slot.winningSymbolSets.Add(null); 

        slot.symbolSets.Capacity = slot.symbolSets.Capacity + 1;
        slot.symbolSets.Add(new SetsWrapper());

        SetsWrapper setsets = slot.symbolSets[slot.symbolSets.Capacity - 1];
        setsets.symbols = new List<int>();
        int an = 0;
        for (int i = 0; i < Name.Length; i++)
        {
            if (Name[i] == name)
            {
                an = i;
            }
        }

        setsets.symbols.Add(an);

        slot.setPays.Capacity = slot.setPays.Capacity + 1;
        slot.setPays.Add(new PaysWrapper());

        PaysWrapper setpays = slot.setPays[slot.setPays.Capacity - 1];
        setpays.pays = new List<float>();
        setpays.pays.Capacity = slot.numberOfReels;
        setpays.anticipate = new List<bool>();
        setpays.anticipate.Capacity = slot.numberOfReels;


        for (int c = 0; c < slot.numberOfReels; c++)
        {
            //print(c);

            if (c > 1)
            {

                if (an < 10)
                {
                    //print("in");
                    //slot.setPays[slot.setPays.Capacity - 1].pays.Add(c + (Level - 1) + an);
                    if (c == 2)
                    {
                        float X = (10f / (10f - (float)an));
                        //print(X);
                        X = (Mathf.Round(X * 100) * 0.01f);
                        slot.setPays[slot.setPays.Capacity - 1].pays.Add(X);
                    }
                    else if (c == 3)
                    {
                        float j = (20f / (10f - (float)an));
                        slot.setPays[slot.setPays.Capacity - 1].pays.Add(Mathf.Round(j * 100.0f) * 0.01f);
                    }
                    else if (c == 4)
                    {
                        slot.setPays[slot.setPays.Capacity - 1].pays.Add((an + 2) * 3);
                    }
                    //print(""+);
                }
                else
                {
                    slot.symbolSets[an].allowWilds = false;
                }
                if (an == 10)
                {
                    //if (c == 4)
                    //{
                    slot.setPays[slot.setPays.Capacity - 1].pays.Add(1);
                    //}
                }
            }
            else { slot.setPays[slot.setPays.Capacity - 1].pays.Add(0); }
            slot.setPays[slot.setPays.Capacity - 1].anticipate.Add(false);

        }

    }

    public void Removing_symbols_set()
    {
        if (slot.symbolSetNames.Count > 0)
        {
            slot.symbolSetNames.RemoveAt(slot.symbolSetNames.Count - 1);
            slot.symbolSetNames.Capacity = slot.symbolSetNames.Capacity - 1;

            slot.symbolSets.RemoveAt(slot.symbolSets.Count - 1);
            slot.symbolSets.Capacity = slot.symbolSets.Capacity - 1;

            slot.setPays.RemoveAt(slot.setPays.Count - 1);
            slot.setPays.Capacity = slot.setPays.Capacity - 1;
        }
        //setFoldouts.RemoveAt(setFoldouts.Count - 1);
    }
    public void Adding_Paylines()
    {
        slot.lines.Capacity = slot.lines.Capacity + 1;
        //print("slot.lines.Capacity" + slot.lines.Capacity);
        slot.lines.Add(new LinesWrapper());

        LinesWrapper setlines = slot.lines[slot.lines.Capacity - 1];
        setlines.positions = new List<int>();
        for (int c = 0; c < slot.numberOfReels; c++)
        {
            slot.lines[slot.lines.Capacity - 1].positions.Add(0);
        }
    }


    public void Adding_Reels()
    {
        slot.numberOfReels++;
        for (int i = 0; i < slot.symbolSetNames.Count; i++)
        {
            slot.setPays[i].pays.Add(0);
            slot.setPays[i].anticipate.Add(false);
        }
        for (int i = 0; i < slot.lines.Count; i++)
        {
            slot.lines[i].positions.Add(0);
        }
    }

    public void Change_BG_Reel_container()
    {
        Sprite a = Resources.Load<Sprite>("Level_Design/" + Level.ToString() + "/BG/" + "b1");
        BG.sprite = a;
        Sprite b = Resources.Load<Sprite>("Level_Design/" + Level.ToString() + "/Frame/" + "f" + Level.ToString());
        Reel_container.sprite = b;
    }
    // Update is called once per frame
    void Update()
    {

    }

    public void Create_lvl_prefab()
    {
        slot.gameObject.transform.parent.gameObject.SetActive(true);
        GameObject a = Duplicate_object(slot.gameObject.transform.parent.gameObject);
        PrefabUtility.CreatePrefab("Assets/Resources/Level_Prefabs/" + Level.ToString() + "/Lvl_container.prefab", a);
        DestroyImmediate(a);
        slot.gameObject.transform.parent.gameObject.SetActive(false);
    }
    public GameObject Duplicate_object(GameObject x)
    {
        GameObject duped = Instantiate(x, x.transform.position, x.transform.rotation, x.transform.parent);
        duped.transform.SetSiblingIndex(x.transform.GetSiblingIndex() + 1);
        return duped;
    }

    public void Change_payline_color()
    {
        var sourse = new StreamReader("Assets/Resources/Level_Design/colorcode.txt");
        var fileContents = sourse.ReadToEnd();
        sourse.Close();
        string[] lines = fileContents.Split("\n"[0]);
        string[] colors_text = lines[Level - 1].Split(","[0]);
        for (int i = 0; i < 2; i++)
        {
            colors_text[i] = "#" + colors_text[i];
        }
        foreach (string co in colors_text)
        {
            //print(co + "\n");
        }
        Color color;
        if (ColorUtility.TryParseHtmlString(colors_text[0], out color))
        {
            slot.GetComponent<SlotLines>().payLineColor1 = color;
            slot.GetComponent<SlotLines>().payLineColor2 = color;
        }
        Color ac;
        if (ColorUtility.TryParseHtmlString(colors_text[1], out ac))
        {
            slot.GetComponent<SlotLines>().strokeColor = ac;
            //print(slot.GetComponent<SlotLines>().strokeColor);
        }



    }
#endif
}
