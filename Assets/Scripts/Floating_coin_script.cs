﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using DG.Tweening;

public class Floating_coin_script : MonoBehaviour
{

    public RectTransform home_panel_coin_rect,down_position;
    public GameObject floating_text;
    public Transform content_homepanel;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    
    public void Move_up(string value_)
    {
        GameObject temp = Instantiate(floating_text, Vector3.zero, Quaternion.identity);
        temp.transform.SetParent(content_homepanel);
        temp.transform.localScale = Vector3.one;
        temp.transform.position = home_panel_coin_rect.position;
        Color cl = temp.GetComponent<TextMeshProUGUI>().color;
        cl.a = 0.5f;
        temp.GetComponent<TextMeshProUGUI>().text = value_;
        temp.GetComponent<TextMeshProUGUI>().DOColor(cl, 1f);
        temp.transform.DOLocalMoveY(temp.transform.localPosition.y + 50, 1f).OnComplete(() => Destroy(temp));
    }

    public void Move_down(string value_)
    {
        GameObject temp = Instantiate(floating_text, Vector3.zero, Quaternion.identity);
        temp.transform.SetParent(content_homepanel);
        temp.transform.localScale = Vector3.one;
        temp.transform.position = down_position.position;
        Color cl = temp.GetComponent<TextMeshProUGUI>().color;
        cl.a = 0.5f;
        temp.GetComponent<TextMeshProUGUI>().text = value_;
        temp.GetComponent<TextMeshProUGUI>().DOColor(cl, 1f);
        temp.transform.DOLocalMoveY(temp.transform.localPosition.y - 50, 1f).OnComplete(() => Destroy(temp));
    }
}
