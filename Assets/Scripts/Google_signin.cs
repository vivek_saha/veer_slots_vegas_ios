﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
//using GooglePlayGames;

using UnityEngine.SocialPlatforms;
using UnityEngine.UI;

public class Google_signin : MonoBehaviour
{

    public static Google_signin Instance;
    public Button login_but;    
    public TextMeshProUGUI Login_Button_text;
    public Sprite log_in, log_out;
    //public Image profilepic;





    void Awake()
    {

        Instance = this;
        //Firebase.Auth.FirebaseAuth auth = Firebase.Auth.FirebaseAuth.DefaultInstance;
        //PlayGamesPlatform.Activate();
    }


    //public void SignIN()
    //{

    //    Social.localUser.Authenticate((bool Success) =>
    //    {
    //        Splash_Panel_scripts.Instance.Loading_screen();
    //        if (Success)
    //        {
    //            Debug.Log ("Sucess!!");
    //            //GooglePlayGames.BasicApi.IPlayGamesClient asd;
    //            //GetUserEmail();
    //            if (PlayerPrefs.GetInt("Google_Login") == 0)
    //            {
    //                PlayerPrefs.SetInt("Google_Login", 1);
    //            }
    //            else
    //            {

    //            }

    //            set_profile_pic();

    //            PlayerPrefs.SetInt("GuestLogin", 0);
    //            set_data();
    //            Send_Data_to_SErver.Instance.Upload_data();
    //            Login_Button.interactable = false;
    //        }
    //        else
    //        {
    //            Debug.Log("Failed!!");
    //            PlayerPrefs.SetInt("Google_Login", 0);
    //            Login_Button.interactable = true;
    //        }
    //    });
    //}

    public void sign_in_failed()
    {
        //Debug.Log("Failed!!");
        PlayerPrefs.SetInt("Google_Login", 0);
        //Login_Button.interactable = true;
        //Login_Button.gameObject.GetComponent<Image>().sprite = log_in;
        Login_Button_text.text = "Login";
    }

    public void sign_in_sucess()
    {
        //Debug.Log("In_sign_in_sucess");

        if (PlayerPrefs.GetInt("Google_Login") == 0)
        {
            PlayerPrefs.SetInt("Google_Login", 1);
        }
        else
        {

        }

        //set_profile_pic();

        PlayerPrefs.SetInt("GuestLogin", 0);
        if (Splash_Panel_scripts.Instance.gameObject.activeSelf)
        {
            set_data();
            Splash_Panel_scripts.Instance.Loading_screen();
        }
        Send_Data_to_SErver.Instance.Upload_data();
        //Login_Button.interactable = false;
        //Login_Button.gameObject.GetComponent<Image>().sprite = log_out;
        Login_Button_text.text = "Logout";
    }


    public void SignOUT()
    {
        //Debug.Log("1st");
        Gamemanager.Instance.home_panel.SetActive(false);
        Gamemanager.Instance.Setting_panel.SetActive(false);
        //Login_Button.gameObject.GetComponent<Image>().sprite = log_in;
        Login_Button_text.text = "Login";
        PlayerPrefs.SetString("Token", string.Empty);
        PlayerPrefs.SetInt("Google_Login", 0);
        PlayerPrefs.SetInt("GuestLogin", 0);
        //Debug.Log("2nd");
        Splash_Panel_scripts.Instance.gameObject.SetActive(true);
        Splash_Panel_scripts.Instance.manual_Start();
    }

    public void set_profile_pic()
    {

        //Texture2D tex;
        ////while(Social.localUser.image==null)
        ////{
        ////    Debug.Log("image not found");
        ////}
        ////Debug.Log("Image found");
        ////tex = Social.localUser.image;
        ////profilepic.sprite = Sprite.Create(tex, new Rect(0f,0f,tex.width,tex.height),new Vector2(0.5f,0.5f));
        //Guest_login_script.Instance.Save_image_to_file(tex);
    }

    public void set_data()
    {


        //Send_Data_to_SErver.Instance.login_type = "google";
        //Send_Data_to_SErver.Instance.fb_id = Social.localUser.id;
        ////Send_Data_to_SErver.Instance.email =PlayGamesPlatform.Instance.GetUserEmail();
        //Send_Data_to_SErver.Instance.name = Social.localUser.userName;
        //Send_Data_to_SErver.Instance.country_code= 
        //Send_Data_to_SErver.Instance.profile_picture_link = PlayGamesPlatform.Instance.GetUserImageUrl();
        Send_Data_to_SErver.Instance.wallet_coin = long.Parse(PlayerPrefs.GetString(("Slot_credits"), "0"));
        Send_Data_to_SErver.Instance.current_xp = PlayerPrefs.GetInt("CurrentXP", 0);
    }

}
