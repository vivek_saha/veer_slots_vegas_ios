﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class Coin_effect_home_panel : MonoBehaviour
{
    public RectTransform coin_container, buy_coins, watch_video, daily_bonus;
    public GameObject coin_to_spin_prefab, coin_object, coin_effect_obj;
    public Transform content;

    public long credits_value;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void on_buy_coin()
    {
        GameObject temp = Instantiate(coin_to_spin_prefab, Vector3.zero, Quaternion.identity);
        temp.transform.SetParent(content);
        temp.transform.localScale = Vector3.one;
        temp.transform.position = buy_coins.position;
        temp.GetComponent<RectTransform>().DOMove(coin_container.position, 01f).OnComplete(() => { Destroy(temp); coin_size_changer(); });
    }
    public void coin_size_changer()
    {
        GetComponent<Floating_coin_script>().Move_up("+" + credits_value);
        Gamemanager.Instance.Setting_vlaues();
        coin_object.transform.DOPunchScale(new Vector3(0.1f, 0.1f), 0.5f).OnComplete(() => Coin_effect());
    }

    public void Coin_effect()
    {
        coin_effect_obj.SetActive(true);
        Invoke("stop_coin_effect", 1f);
    }

    public void stop_coin_effect()
    {
        coin_effect_obj.SetActive(false);
    }

    public void on_bonus_add()
    {
        GameObject temp = Instantiate(coin_to_spin_prefab, Vector3.zero, Quaternion.identity);
        temp.transform.SetParent(content);
        temp.transform.localScale = Vector3.one;
        temp.transform.position = daily_bonus.position;
        temp.GetComponent<RectTransform>().DOMove(coin_container.position, 01f).OnComplete(() => { Destroy(temp); coin_size_changer(); });


    }

    public void on_watch_video_add()
    {
        GameObject temp = Instantiate(coin_to_spin_prefab, Vector3.zero, Quaternion.identity);
        temp.transform.SetParent(content);
        temp.transform.localScale = Vector3.one;
        temp.transform.position = watch_video.position;
        temp.GetComponent<RectTransform>().DOMove(coin_container.position, 01f).OnComplete(() => { Destroy(temp); coin_size_changer(); });

    }
}
