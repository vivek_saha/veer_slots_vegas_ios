﻿// Brad Lima - 11/2019
//
// This is the customized Callbacks script for Beach Days Slot
//
using UnityEngine;
using System.Collections;

public class callbacks_managers_forslot : MonoBehaviour
{

	[HideInInspector]
	private Slot slot;
	//public BeachDaysGUI deck;
	private Leveling leveling;
	private GameObject lvl_manager;
	string win_setname;


	bool for_5OK = true,bonus=true,xp_bool=true;
	//public Scatters scatters;

	void Start()
	{
		slot = GetComponent<Slot>();
		lvl_manager = GameObject.FindGameObjectWithTag("Lvl_manager");
		leveling = lvl_manager.GetComponent<Lvl_manager>().leveling;
	}

	#region Enable/Disable
	void OnEnable()
	{
		//GameObject a = GameObject.FindGameObjectWithTag("Slot");
		//slot = a.GetComponent<Slot>();
		slot = GetComponent<Slot>();


		Slot.OnSlotStateChangeTo += OnSlotStateChangeTo;
		Slot.OnSlotStateChangeFrom += OnSlotStateChangeFrom;

		Slot.OnSpinBegin += OnSpinBegin;
		Slot.OnSpinInsufficentCredits += OnSpinInsufficentCredits;
		Slot.OnSpinSnap += OnSpinSnap;
		Slot.OnSpinDone += OnSpinDone;
		Slot.OnSpinDoneNoWins += OnSpinDoneNoWins;
		Slot.OnReelLand += OnReelLand;

		Slot.OnLineWinComputed += OnLineWinComputed;
		Slot.OnLineWinDisplayed += OnLineWinDisplayed;
		Slot.OnAllWinsComputed += OnAllWinsComputed;
		Slot.OnScatterSymbolHit += OnScatterSymbolHit;
		Slot.OnAnticipationScatterBegin += OnAnticipationScatterBegin;

		Slot.OnScatterSymbolLanded += OnScatterSymbolLanded;
		Slot.OnWinDisplayedCycle += OnWinDisplayedCycle;
		Slot.OnLinkedSymbolLanded += OnLinkedSymbolLanded;

		Slot.OnBeginCreditWinCountOff += OnBeginCreditWinCountOff;
		Slot.OnCompletedCreditCountOff += OnCompletedCreditCountOff;

		Slot.OnSymbolReturningToPool += OnSymbolReturningToPool;
	}

	#endregion

	void OnDisable()
	{

		Slot.OnSlotStateChangeTo -= OnSlotStateChangeTo;
		Slot.OnSlotStateChangeFrom -= OnSlotStateChangeFrom;

		Slot.OnSpinBegin -= OnSpinBegin;
		Slot.OnSpinInsufficentCredits -= OnSpinInsufficentCredits;
		Slot.OnSpinSnap -= OnSpinSnap;
		Slot.OnSpinDone -= OnSpinDone;
		Slot.OnSpinDoneNoWins -= OnSpinDoneNoWins;

		Slot.OnLineWinDisplayed -= OnLineWinDisplayed;
		Slot.OnAllWinsComputed -= OnAllWinsComputed;

		Slot.OnScatterSymbolHit -= OnScatterSymbolHit;
		Slot.OnAnticipationScatterBegin -= OnAnticipationScatterBegin;

		Slot.OnScatterSymbolLanded -= OnScatterSymbolLanded;
		Slot.OnWinDisplayedCycle -= OnWinDisplayedCycle;
		Slot.OnLinkedSymbolLanded -= OnLinkedSymbolLanded;

		Slot.OnBeginCreditWinCountOff -= OnBeginCreditWinCountOff;
		Slot.OnCompletedCreditCountOff -= OnCompletedCreditCountOff;

		Slot.OnSymbolReturningToPool -= OnSymbolReturningToPool;

	}

	#region Update Callback

	private void OnSlotUpdate()
	{
	}
	#endregion


	#region State Callbacks 

	private void OnSlotStateChangeFrom(SlotState state)
	{
		slot.log("onSlotStateChangeFrom " + state);
		switch (state)
		{
			case SlotState.playingwins:
				break;
			case SlotState.ready:
				break;
			case SlotState.snapping:
				break;
			case SlotState.spinning:
				break;
		}
	}
	public void OnSlotStateChangeTo(SlotState state)
	{
		slot.log("OnSlotStateChangeTo " + state);
		switch (state)
		{
			case SlotState.playingwins:
				break;
			case SlotState.ready:
				break;
			case SlotState.snapping:
				break;
			case SlotState.spinning:
				break;
		}
	}
	#endregion

	#region Spin Callbacks
	private void OnSpinBegin(SlotWinData data)
	{
		Audio_manager.Instance.slot_win.Stop();
		//Audio_manager.Instance.slot_sound.clip = Audio_manager.Instance.spin;
		slot.refs.lines.hideLines();
		//leveling.AwardXp(slot.refs.credits.totalBet());
		slot.log("OnSpinBegin Callback");
		for_5OK = true;
		bonus = true;
		xp_bool = true;
	}

	private void OnSpinInsufficentCredits()
	{
		slot.log("OnSpinInsufficentCredits Callback");
		lvl_manager.GetComponent<Lvl_manager>().notenough_money_Function();
	}

	void OnReelLand(long obj)
	{
		Audio_manager.Instance.slot_sound.clip = Audio_manager.Instance.reel_stop;
		Audio_manager.Instance.slot_sound.Play();
		StartCoroutine("Change_slot_sound");
    }


	IEnumerator Change_slot_sound()
	{
        //AudioSource audio = GetComponent<AudioSource>();

        //audio.Play();
        yield return new WaitForSeconds(Audio_manager.Instance.slot_sound.clip.length);
		Audio_manager.Instance.reel_sound.volume -= 0.05f;
		//Audio_manager.Instance.slot_sound.clip = Audio_manager.Instance.spin;
		//audio.clip = ;
		//audio.Play();
	}
	private void OnSpinSnap()
	{
		slot.log("OnSpinSnap Callback");
		Audio_manager.Instance.reel_sound.volume = 0.4f;
		//Audio_manager.Instance.slot_sound.clip = Audio_manager.Instance.spin;

	}

	private void OnSpinDone(long totalWon, long timesWin)
	{
		Audio_manager.Instance.reel_sound.Stop();
		Audio_manager.Instance.reel_sound.volume = 0.4f;
		//if (win_setname == "Bonus")
		//{
		//	//leveling.AwardXp(Gamemanager.Instance.scatter_default_value[Lvl_manager.Instance.lvl_theme-1]);
		//	slot.refs.credits.lastWin = Gamemanager.Instance.scatter_default_value[Lvl_manager.Instance.lvl_theme - 1];

		//}
		//else
		//{
		//	//leveling.AwardXp(slot.refs.credits.totalBet());
		//}
		//Audio_manager.Instance.slot_sound.clip = Audio_manager.Instance.spin;
		if (xp_bool)
		{
			leveling.AwardXp(slot.refs.credits.totalBet());
		}
		lvl_manager.GetComponent<Lvl_manager>().updateLevel();
		if (totalWon > 0)
		{
			lvl_manager.GetComponent<Lvl_manager>().totalwon_lvl = totalWon;
			if (lvl_manager.GetComponent<Lvl_manager>().fiveofkind_panel.activeSelf)
			{
				Invoke("wait_for_fok",4f);
			}
			else
			{
				lvl_manager.GetComponent<Spin_coin_script>().win_effect();
			}
		}
        lvl_manager.GetComponent<Lvl_manager>().updateUI();
        //Debug.Log("OnSpinDone Callback");
	}

	public void wait_for_fok()
    {
		lvl_manager.GetComponent<Spin_coin_script>().win_effect();
	}

	private void OnSpinDoneNoWins()
	{
		Audio_manager.Instance.reel_sound.Stop();
		//Audio_manager.Instance.reel_sound.volume = 0.4f;
		Audio_manager.Instance.reel_sound.volume = 0.4f;
		//Audio_manager.Instance.slot_sound.clip = Audio_manager.Instance.spin;
		if (xp_bool)
		{
			leveling.AwardXp(slot.refs.credits.totalBet());
		}//lvl_manager.GetComponent<Lvl_manager>().updateLevel();
		slot.log("OnSpinDoneNoWins Callback");
	}

	#endregion

	#region Win Callbacks
	private void OnLineWinComputed(SlotWinData win)
	{
		if (!Lvl_manager.Instance.fiveofkind_panel.activeSelf)
		{
			Audio_manager.Instance.slot_win.clip = Audio_manager.Instance.slot_win_clip;
			Audio_manager.Instance.slot_win.Play();
		}
		slot.log("OnLineWinComputed Callback");

		int matches = 5;
		while (matches < 8)
		{
			// search line results for the set name and the matches
			int jackPot = slot.refs.compute.lineResultData.FindIndex(item => item.setName == "Jackpot" && item.matches == matches);

			if (jackPot > -1)
			{
				//win.setPaid(scatters.GetScatter(matches - 5));
				slot.refs.compute.lineResultData[jackPot] = win;
				//scatters.ResetScatter(matches - 5);
				matches = 8;
			}
			matches++;
		}

		slot.log("win line " + win.lineNumber + " :: set: " + win.setName + " (" + win.setIndex + ") paid: " + win.paid + " matches: " + win.matches);
		win_setname = win.setName;
		if(win.matches==5)
        {
			if (win.setName == "Bonus")
			{
				if (bonus)
				{
					Lvl_manager.Instance.Jackpot_win();
					bonus = false;
				}
			}
			else if (for_5OK)
			{
				Lvl_manager.Instance.fiveofakind();
				for_5OK = false;
			}
		}

		
	}

	private void OnLineWinDisplayed(SlotWinData win, bool isFirstLoop)
	{
		// Itterate through symbols that make up the win
		foreach (GameObject symbol in win.symbols)
		{
			// if there is an animator component, play the win animation
			if (symbol.GetComponent<Animator>())
				symbol.GetComponent<Animator>().SetTrigger("playwin");
		}
		slot.log("OnLineWinDisplayed Callback");
		slot.log("win line " + win.lineNumber + " :: set: " + win.setName + " (" + win.setIndex + ") paid: " + win.paid + " matches: " + win.matches);
		//Debug.Log("win line " + win.lineNumber + " :: set: " + win.setName + " (" + win.setIndex + ") paid: " + win.paid + " matches: " + win.matches);
	}

	private void OnAllWinsComputed(SlotWinSpin win, long timesBet)
	{
		slot.log("OnAllWinsComputed Callback");
	}

	private void OnScatterSymbolLanded(GameObject symbol, int count)
	{

		// we don't want to play the scatter animations if the reels are snapping
		if (slot.state == SlotState.snapping) return;

		// trigger animation when scatter symbol lands if there is one
		Animator anim = symbol.GetComponent<Animator>();
		if (anim)
		{
			anim.SetTrigger("playwin");
		}

	}

	private void OnScatterSymbolHit(SlotScatterHitData hit)
	{
		slot.log("OnScatterSymbolHit Callback");
	}

	private void OnAnticipationScatterBegin(SlotScatterHitData hit)
	{
		slot.log("OnAnticipationScatterBegin Callback");
	}

	public void OnLinkedSymbolLanded(int reel, string linkName)
	{
		slot.log("OnLinkedSymbolLanded:" + reel + " : " + linkName);
		Debug.Log("OnLinkedSymbolLanded:" + reel + " : " + linkName);
	}

	void OnWinDisplayedCycle(int count)
	{
		slot.log("OnWinDisplayedCycle Callback");
	}

	void OnBeginCreditWinCountOff(long obj)
	{

	}

	void OnCompletedCreditCountOff(long obj)
	{

	}

	void OnSymbolReturningToPool(GameObject obj)
	{

	}

	#endregion
}
