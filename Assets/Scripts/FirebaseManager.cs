//using System;
//using System.Threading.Tasks;
//using UnityEngine;
//using Firebase;
//using Firebase.Extensions;
//using Firebase.Messaging;

//public class FirebaseManager : MonoBehaviour
//{
//    private static FirebaseManager firebaseManager;

//    private string topic = "veerslotgametopic";

//    private DependencyStatus dependencyStatus = DependencyStatus.UnavailableOther;
//    protected bool isFirebaseInitialized = false;

//    private void Awake()
//    {
//        if (firebaseManager == null)
//        {
//            firebaseManager = this;
//            DontDestroyOnLoad(gameObject);
//        }
//        else
//        {
//            Destroy(gameObject);
//        }
//    }

//    private void Start()
//    {
//        FirebaseApp.CheckAndFixDependenciesAsync().ContinueWithOnMainThread(task => {
//            dependencyStatus = task.Result;
//            if (dependencyStatus == DependencyStatus.Available)
//            {
//                InitializeFirebase();
//            }
//            else
//            {
//                Debug.LogError("Could not resolve all Firebase dependencies: " + dependencyStatus);
//            }
//        });
//    }

//    void InitializeFirebase()
//    {
//        //FirebaseMessaging.MessageReceived += OnMessageReceived;
//        //FirebaseMessaging.TokenReceived += OnTokenReceived;
//        FirebaseMessaging.SubscribeAsync(topic).ContinueWithOnMainThread(task => {
//            LogTaskCompletion(task, "SubscribeAsync");
//        });
//        Debug.Log("Firebase Messaging Initialized");

//        // This will display the prompt to request permission to receive
//        // notifications if the prompt has not already been displayed before. (If
//        // the user already responded to the prompt, thier decision is cached by
//        // the OS and can be changed in the OS settings).
//        FirebaseMessaging.RequestPermissionAsync().ContinueWithOnMainThread(
//          task => {
//              LogTaskCompletion(task, "RequestPermissionAsync");
//          }
//        );
//        isFirebaseInitialized = true;
//    }

//    private bool LogTaskCompletion(Task task, string operation)
//    {
//        bool complete = false;
//        if (task.IsCanceled)
//        {
//            Debug.Log(operation + " canceled.");
//        }
//        else if (task.IsFaulted)
//        {
//            Debug.Log(operation + " encounted an error.");
//            foreach (Exception exception in task.Exception.Flatten().InnerExceptions)
//            {
//                string errorCode = "";
//                Firebase.FirebaseException firebaseEx = exception as Firebase.FirebaseException;
//                if (firebaseEx != null)
//                {
//                    errorCode = String.Format("Error.{0}: ",
//                      ((Firebase.Messaging.Error)firebaseEx.ErrorCode).ToString());
//                }
//                Debug.Log(errorCode + exception.ToString());
//            }
//        }
//        else if (task.IsCompleted)
//        {
//            Debug.Log(operation + " completed");
//            complete = true;
//        }
//        return complete;
//    }

//    /*public virtual void OnMessageReceived(object sender, Firebase.Messaging.MessageReceivedEventArgs e)
//    {
//        Debug.Log("Received a new message");
//        var notification = e.Message.Notification;
//        if (notification != null)
//        {
//            Debug.Log("title: " + notification.Title);
//            Debug.Log("body: " + notification.Body);
//            var android = notification.Android;
//            if (android != null)
//            {
//                Debug.Log("android channel_id: " + android.ChannelId);
//            }
//        }
//        if (e.Message.From.Length > 0)
//            Debug.Log("from: " + e.Message.From);
//        if (e.Message.Link != null)
//        {
//            Debug.Log("link: " + e.Message.Link.ToString());
//        }
//        if (e.Message.Data.Count > 0)
//        {
//            Debug.Log("data:");
//            foreach (System.Collections.Generic.KeyValuePair<string, string> iter in
//                     e.Message.Data)
//            {
//                Debug.Log("  " + iter.Key + ": " + iter.Value);
//            }
//        }
//    }*/

//    /*public virtual void OnTokenReceived(object sender, Firebase.Messaging.TokenReceivedEventArgs token)
//    {
//        Debug.Log("Received Registration Token: " + token.Token);
//    }*/

//}
