﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Threading.Tasks;
//using Firebase;
//using Firebase.Auth;
//using Firebase.Extensions;
//using Google;
//using UnityEngine;
//using UnityEngine.UI;

//public class GoogleSignInDemo : MonoBehaviour
//{
//    //public Text infoText;
//    public string webClientId = "<your client id here>";

//    private FirebaseAuth auth;
//    private GoogleSignInConfiguration configuration;

//    public Uri url;

//    private void Awake()
//    {
//        //auth = Firebase.Auth.FirebaseAuth.DefaultInstance;
//        configuration = new GoogleSignInConfiguration { WebClientId = webClientId, RequestEmail = true, RequestIdToken = true };
//        CheckFirebaseDependencies();
//    }

//    private void CheckFirebaseDependencies()
//    {
//        FirebaseApp.CheckAndFixDependenciesAsync().ContinueWith(task =>
//        {
//            if (task.IsCompleted)
//            {
//                if (task.Result == DependencyStatus.Available)
//                    auth = FirebaseAuth.DefaultInstance;
//                else
//                    AddToInformation("Could not resolve all Firebase dependencies: " + task.Result.ToString());
//            }
//            else
//            {
//                AddToInformation("Dependency check was not completed. Error : " + task.Exception.Message);
//            }
//        });
//    }

//    public void SignInWithGoogle() { OnSignIn(); }
//    public void SignOutFromGoogle() { OnSignOut(); }

//    private void OnSignIn()
//    {
//        GoogleSignIn.Configuration = configuration;
//        GoogleSignIn.Configuration.UseGameSignIn = false;
//        GoogleSignIn.Configuration.RequestIdToken = true;
//        AddToInformation("Calling SignIn");

//        GoogleSignIn.DefaultInstance.SignIn().ContinueWithOnMainThread(OnAuthenticationFinished);
//    }

//    private void OnSignOut()
//    {
//        AddToInformation("Calling SignOut");
//        GoogleSignIn.DefaultInstance.SignOut();
//        Google_signin.Instance.SignOUT();
//    }

//    public void OnDisconnect()
//    {
//        AddToInformation("Calling Disconnect");
//        GoogleSignIn.DefaultInstance.Disconnect();
//    }

//    internal void OnAuthenticationFinished(Task<GoogleSignInUser> task)
//    {
//        if (task.IsFaulted)
//        {

//            using (IEnumerator<Exception> enumerator = task.Exception.InnerExceptions.GetEnumerator())
//            {
//                if (enumerator.MoveNext())
//                {
//                    GoogleSignIn.SignInException error = (GoogleSignIn.SignInException)enumerator.Current;
//                    AddToInformation("Got Error: " + error.Status + " " + error.Message);
//                }
//                else
//                {
//                    AddToInformation("Got Unexpected Exception?!?" + task.Exception);
//                }
//            Google_signin.Instance.sign_in_failed();
//            }
//        }
//        else if (task.IsCanceled)
//        {
//            Google_signin.Instance.sign_in_failed();
//            AddToInformation("Canceled");
//        }
//        else
//        {
//            //Debug.Log("here_onauth");
//            //AddToInformation("Welcome: " + task.Result.DisplayName + "!");
//            //AddToInformation("Email = " + task.Result.Email);
//            //AddToInformation("Google ID Token = " + task.Result.IdToken);
//            //AddToInformation("Email = " + task.Result.Email);
//            Send_Data_to_SErver.Instance.login_type = "google";
//            Send_Data_to_SErver.Instance.fb_id = task.Result.UserId;
//            //Debug.Log("Send_Data_to_SErver.Instance.fb_id" + Send_Data_to_SErver.Instance.fb_id);
//            Send_Data_to_SErver.Instance.email = task.Result.Email;
//            //Debug.Log("Send_Data_to_SErver.Instance.email" + Send_Data_to_SErver.Instance.email);
//            string temp = task.Result.DisplayName.Substring(0, 9);
//            //Debug.Log
//            Send_Data_to_SErver.Instance.Name = temp;
//            //Debug.Log("Send_Data_to_SErver.Instance.Name" + Send_Data_to_SErver.Instance.Name);
//            url = task.Result.ImageUrl;
//            //Debug.Log("url" + url);
//            Send_Data_to_SErver.Instance.profile_picture_link = url.ToString();
//            //Set_User_data.Instance.set_google_profile_link(url.ToString());
//            //Guest_login_script.Instance.Save_image_to_file(Guest_login_script.Instance.temp_tex);
//            SignInWithGoogleOnFirebase(task.Result.IdToken);
//            Google_signin.Instance.sign_in_sucess();
//        }
//    }

//    private void SignInWithGoogleOnFirebase(string idToken)
//    {
//        Credential credential = GoogleAuthProvider.GetCredential(idToken, null);

//        auth.SignInWithCredentialAsync(credential).ContinueWith(task =>
//        {
//            AggregateException ex = task.Exception;
//            if (ex != null)
//            {
//                if (ex.InnerExceptions[0] is FirebaseException inner && (inner.ErrorCode != 0))
//                    AddToInformation("\nError code = " + inner.ErrorCode + " Message = " + inner.Message);
//            }
//            else
//            {
//                AddToInformation("Sign In Successful.");
//            }
//        });
//    }

//    public void OnSignInSilently()
//    {
//        //GoogleSignIn.Configuration = configuration;
//        GoogleSignIn.Configuration.UseGameSignIn = false;
//        GoogleSignIn.Configuration.RequestIdToken = true;
//        AddToInformation("Calling SignIn Silently");

//        GoogleSignIn.DefaultInstance.SignInSilently().ContinueWith(OnAuthenticationFinished);
//    }

//    public void OnGamesSignIn()
//    {
//        //GoogleSignIn.Configuration = configuration;
//        GoogleSignIn.Configuration.UseGameSignIn = true;
//        GoogleSignIn.Configuration.RequestIdToken = false;

//        AddToInformation("Calling Games SignIn");

//        GoogleSignIn.DefaultInstance.SignIn().ContinueWith(OnAuthenticationFinished);
//    }

//    private void AddToInformation(string str) { Debug.Log(str); }



//    public void g_plus_button_clcik()
//    {
//        if (PlayerPrefs.GetInt("Google_Login") == 0)
//        {
//            SignInWithGoogle();
//        }
//        else
//        {
//            SignOutFromGoogle();
//        }
//    }
//}