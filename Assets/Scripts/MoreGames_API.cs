﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SimpleJSON;
using UnityEngine.Networking;
using System.IO;
using System;
using IAP;
using TMPro;
using UnityEngine.UI;

public class MoreGames_API : MonoBehaviour
{

    public static MoreGames_API Instance;

    private string url = "http://134.209.103.120/MoreApp/api/recent/ios/game";

    // resulting JSON from an API request
    public JSONNode jsonResult;

    // create the web request and download handler
    //UnityWebRequest webReq = new UnityWebRequest();

    string rawJson;
    public GameObject more_app_but_conatiner;
    public Transform more_app_content;




    private string logo_link_1 = "http://134.209.103.120/MoreApp/public";
    //public bool success;
    public string[] Name;
    public string[] logo_link_2;
    public string[] game_link;
    private string[] package_id;
    public Texture2D[] icons;
    Texture2D temp_image;



    public bool req_call = true;

    private void Awake()
    {
        Instance = this;

    }


    private void Start()
    {
        StartCoroutine("GetData");
    }




    // sends an API request - returns a JSON file
    void Manual_Start()
    {

        jsonResult = JSON.Parse(rawJson);
        Set_Json_data();
        Get_icons();

    }



    public void Get_icons()
    {
        //icons = new Texture2D[logo_link_2.Length];

        for (int i = 0; i < logo_link_2.Length; i++)
        {


             GameObject a=  Instantiate(more_app_but_conatiner, Vector3.zero, Quaternion.identity);
            a.transform.parent = more_app_content.transform;
            a.transform.localScale = new Vector3(1, 1, 1);
            a.GetComponent<More_app_but_script>().Game_name.text = Name[i];
            a.GetComponent<More_app_but_script>().app_link = game_link[i];
            a.GetComponent<More_app_but_script>().image_url = logo_link_1 + logo_link_2[i];
            //a.GetComponent<More_app_but_script>().Get_image();
           
        }
    }

    public void Set_Json_data()
    {

        int m = 0;
        foreach (JSONNode message in jsonResult["recent_game"])
        {
            if (Application.identifier != message["package"].Value)
            {
                m++;
            }
        }
        Name = new string[m];
        logo_link_2 = new string[m];
        game_link = new string[m];
        package_id = new string[m];


        int counter = 0;
        foreach (JSONNode message in jsonResult["recent_game"])
        {
            if (Application.identifier != message["package"].Value)
            {
                Name[counter] = message["name"].Value;
                logo_link_2[counter] = message["logo"].Value;
                game_link[counter] = message["ios_link"].Value;
                package_id[counter] = message["ios_package"].Value;

                counter++;
            }
        }

    }







    IEnumerator GetData()
    {
        UnityWebRequest www = UnityWebRequest.Get(url);
        req_call = false;
        yield return www.Send();
        //Debug.Log(www);
        if (www.isNetworkError)
        {
            Debug.Log(www.error);

            Check_Internet_connection();
            req_call = true;
            
        }
        else
        {
            close_Retry_panel();
            // Show results as text
            //Debug.Log(www.downloadHandler.text);
            rawJson = www.downloadHandler.text;
            Manual_Start();
            // Or retrieve results as binary data
            // byte[] results = www.downloadHandler.data;
        }
    }
    //IEnumerator GetData_url(string urls)
    //{
    //    UnityWebRequest www = UnityWebRequestTexture.GetTexture(urls);
    //    //req_call = false;
    //    yield return www.Send();
    //    Debug.Log(www);
    //    if (www.isNetworkError)
    //    {
    //        Debug.Log(www.error);
    //        //Get_API_Data_IAP.Instance.
    //    }
    //    else
    //    {
    //        temp_image = DownloadHandlerTexture.GetContent(www);
    //        icons[0] = temp_image;
    //        //temp_image = ((DownloadHandlerTexture)www.downloadHandler).texture;
    //        print(DownloadHandlerTexture.GetContent(www));
    //    }


    //    //WWW wwwLoader = new WWW(urls);   // create WWW object pointing to the url
    //    //yield return wwwLoader;         // start loading whatever in that url ( delay happens here )

    //    //Debug.Log("Loaded");
    //    //temp_image = wwwLoader.texture;
    //    //thisRenderer.material.color = Color.white;              // set white
    //    //thisRenderer.material.mainTexture = wwwLoader.texture;
    //}



    //public static IAP_Data CreateFromJSON(string jsonString)
    //{
    //    return JsonUtility.FromJson<IAP_Data>(jsonString);
    //}

    // Update is called once per frame
    void Update()
    {
        //if (req_call)
        //{
        //    StartCoroutine("GetData");
        //}
    }
    public void Check_Internet_connection()
    {
        StartCoroutine(Ck_net(isConnected =>
        {
            if (isConnected)
            {
                Debug.Log("Internet Available!");
                //return true;
                if (!Splash_Panel_scripts.Instance.Loading_panel.activeSelf)
                {
                    Splash_Panel_scripts.Instance.Notice_panel.SetActive(true);
                    Splash_Panel_scripts.Instance.Something_went_wrong_panel.GetComponentInChildren<Button>().onClick.AddListener(() => Retry_connection());
                    Splash_Panel_scripts.Instance.Something_went_wrong_panel.SetActive(true);
                }
            }
            else
            {
                Debug.Log("Internet Not Available");
                //return  false;
                if (!Splash_Panel_scripts.Instance.Loading_panel.activeSelf)
                {
                    Splash_Panel_scripts.Instance.Notice_panel.SetActive(true);
                    Splash_Panel_scripts.Instance.no_internet_connection_panel.GetComponentInChildren<Button>().onClick.AddListener(() => Retry_connection());
                    Splash_Panel_scripts.Instance.no_internet_connection_panel.SetActive(true);
                }
            }
        }));
    }

    public IEnumerator Ck_net(Action<bool> syncResult)
    {
        const string echoServer = "http://google.com";

        bool result;
        using (var request = UnityWebRequest.Head(echoServer))
        {
            request.timeout = 0;
            yield return request.SendWebRequest();
            result = !request.isNetworkError && !request.isHttpError && request.responseCode == 200;
        }
        syncResult(result);
    }
    //public bool check_net()
    //{
    //    WWW www = new WWW("http://www.google.com");
    //    //yield return www;
    //    if (www.error != null)
    //    {
    //        return false;
    //    }
    //    else
    //    {
    //        return true;
    //    }

    //}
    public void Retry_connection()
    {
        StartCoroutine("GetData");
        Splash_Panel_scripts.Instance.Something_went_wrong_panel.SetActive(false);
        Splash_Panel_scripts.Instance.no_internet_connection_panel.SetActive(false);
        Splash_Panel_scripts.Instance.Loading_panel.SetActive(true);
        StartCoroutine("Retry_connection_coroutine");
    }
    IEnumerator Retry_connection_coroutine()
    {
        yield return new WaitForSeconds(3f);
        //StartCoroutine("check_connection");
        Splash_Panel_scripts.Instance.Loading_panel.SetActive(false);
        Splash_Panel_scripts.Instance.Notice_panel.SetActive(false);
        StartCoroutine("GetData");
    }
    public void close_Retry_panel()
    {
        Splash_Panel_scripts.Instance.Something_went_wrong_panel.SetActive(false);
        Splash_Panel_scripts.Instance.no_internet_connection_panel.SetActive(false);
        Splash_Panel_scripts.Instance.Loading_panel.SetActive(false);
        Splash_Panel_scripts.Instance.Notice_panel.SetActive(false);
    }


}