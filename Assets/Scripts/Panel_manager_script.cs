﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Panel_manager_script : MonoBehaviour
{

   // public GameObject game_rules_panel, Paytable_Panel, LeaderBoard_panel, Shop_panel, MEnu_Panel, More_app_panel, Settings_Panel, Not_enough_money_panel, Exit_panel;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void onclose(GameObject ac)
    {
        StartCoroutine("close_panel", ac);
    }

    IEnumerator close_panel(GameObject sc)
    {
        yield return new WaitForSeconds(0.7f);
        sc.SetActive(false);
    }
}
