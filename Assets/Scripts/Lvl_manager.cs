﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System.Globalization;
using System;
using DG.Tweening;
using System.IO;
//using System.Globalization;

public class Lvl_manager : MonoBehaviour
{
    public static Lvl_manager Instance;

    [Header("GameObject")]
    public GameObject lvl_up_panel;
    public GameObject ob_5OK;
    public GameObject fiveofkind_panel;
    public GameObject Gameplay_panel;
    public GameObject Not_enough_Money_Panel;
    GameObject lvl_theme_container_holder;
    public GameObject Jackpot_panel;
    public GameObject win_box;
    public GameObject win_box2;

    public Slot slot;

    [Header("TextMeshProUGUI")]
    public TextMeshProUGUI bet;
    public TextMeshProUGUI betperline;
    public TextMeshProUGUI lines;
    public TextMeshProUGUI won;
    public TextMeshProUGUI credits;
    public TextMeshProUGUI scatter;
    public TextMeshProUGUI lvl_up_reward;
    public TextMeshProUGUI level;
    public TextMeshProUGUI lvl_up_lvl_value;
    public TextMeshProUGUI Jackpot_value;

    [Header("Sprite")]
    public Sprite win_but;
    public Sprite normal_but;
    // public Sprite Lvl_icon_level_up_panel;

    [Header("Image")]
    public Image Lvl_icon_level_up_panel;

    [Header("Scripts")]
    public Leveling leveling;
    public Scatters scatters;

    List<int> symbols_frequency;

    [Header("Int")]
    public int Level_bonusMultiplier = 200;
    public int lvl_theme;
    public int lvl_cap_server = 20;
    private int old_lvl;
    public long totalwon_lvl;

    [Header("Bool")]
    public bool Fiveofkindison = false;
    bool remaininglvlup = false;

    [HideInInspector]
    [Header("Button[]")]
    public Button[] Payline_buts;

    NumberFormatInfo nfi;
    public float duration = 5;

    long target_coins, current_coins;

    private bool isCoroutineExecuting = false;

    GameObject temp_5ok;

    public AssetBundle bundleloadrequest;

    long temp_amont;
    //NumberFormatInfo nfi;
    private void Awake()
    {
        Instance = this;
    }

    void Start()
    {
        nfi = new CultureInfo("en-US", false).NumberFormat;
        nfi.CurrencyDecimalDigits = 0;
        nfi.NumberGroupSeparator = " ";
        leveling = GetComponent<Leveling>();
        //nfi = (NumberFormatInfo)CultureInfo.InvariantCulture.NumberFormat.Clone();
        //after_lvl_add_start();
    }

    public void after_lvl_add_start()
    {
        GameObject a = GameObject.FindGameObjectWithTag("Slot");
        slot = a.GetComponent<Slot>();
        //print(slot);
        //leveling.manual_start();
        scatters.Start_Scatters();
        GetComponent<SLot_butttons_functions>().start_slot_buttons_functions();
        slot.refs.credits.credits = long.Parse(PlayerPrefs.GetString("Slot_credits"));

        symbols_frequency = slot.symbolFrequencies;

        int x = symbols_frequency.Count;

        reset_win_but();
        old_lvl = leveling.GetLevel();
        updateLevel();
        updateUI();
        changein_lines();

        Gameplay_panel.SetActive(true);
        scatters.Start_Scatters();
        GetComponent<Paytable_Script>().start_paytable_script();

        //if(lvl_theme==5)
        //{
        GetComponent<Level_Effects>().Show_particle();
        //}
        totalwon_lvl = 0;
    }


    public void Add_lvl_theme(int _lvltheme)
    {
        lvl_theme = _lvltheme;
        GameObject x = null;
        if (lvl_theme <= lvl_cap_server)
        {
            x = Resources.Load<GameObject>("Level_Prefabs/" + lvl_theme.ToString() + "/Lvl_container");
        }
        else
        {
            //if (Gamemanager.Instance.Unlocked_all_lvl_test)
            //{
            //    x = Resources.Load<GameObject>("Level_Prefabs/" + lvl_theme.ToString() + "/Lvl_container");
            //}
            //else
            //{
               


               //DownloadClass.Instance.Start_download();
                if (bundleloadrequest != null)
                {
                    bundleloadrequest.Unload(true);
                }

                //bundleloadrequest.Unload(true);
                bundleloadrequest = AssetBundle.LoadFromFile(Application.persistentDataPath + "/Levels/" + lvl_theme + ".lvl");

                x = bundleloadrequest.LoadAsset<GameObject>("Lvl_container");
            //}
        }
        lvl_theme_container_holder = Instantiate(x);
        after_lvl_add_start();
    }

    void Update()
    {
        updateScatters();
        if (Gameplay_panel.activeSelf)
        {
            switch (slot.state)
            {
                case SlotState.playingwins:
                    if (!holdspinscript.Instance.autostart)
                    {
                        enable_winline_but();
                    }
                    if (slot.refs.wins.currentWin == null) return;

                    if (slot.refs.wins.isBetweenWins())
                    {

                        //winReadout.text = "";
                    }
                    else
                    {
                        //print(slot.refs.wins.currentWin.readout.ToString());
                    }//winReadout.text = slot.refs.wins.currentWin.readout.ToString();
                    updateWon();
                    updateCredits();
                    updatebetperline();

                    break;
                case SlotState.ready:
                    if (!holdspinscript.Instance.autostart)
                    {
                        enable_winline_but();
                    }
                    break;
                case SlotState.spinning:
                    if (!holdspinscript.Instance.autostart)
                    {
                        disable_winline_but();
                    }
                    won.text = "GOOD LUCK!";
                    //print("");
                    //winReadout.text = "";

                    break;

                default:
                    won.text = "0";
                    //print("");
                    //updateUI();
                    //winReadout.text = "";

                    break;
            }
        }
        if (Jackpot_panel.activeSelf)
        {
            if (current_coins != target_coins)
            {
                current_coins = (long)Mathf.Lerp(current_coins, target_coins, duration * Time.unscaledDeltaTime);
                Jackpot_value.text = AbbrevationUtility.AbbreviateNumber(current_coins).ToString() + " COINS";
            }
        }
    }


    public void Select_Line(int l)
    {
        Disable_Buttons(l);
        slot.GetComponent<SlotCredits>().SetLinesPlayed(l);
        updateUI();
        //print("selected line"+l);
    }

    public void changein_lines()
    {
        int x = slot.refs.credits.linesPlayed;
        Select_Line(x);
    }

    public void Disable_Buttons(int n)
    {
        for (int i = 0; i < Payline_buts.Length; i++)
        {
            if (i >= n)
            {
                Payline_buts[i].image.color = new Color(Payline_buts[i].image.color.r, Payline_buts[i].image.color.g, Payline_buts[i].image.color.b, 0.5f);
            }
            else
            {
                Payline_buts[i].image.color = new Color(Payline_buts[i].image.color.r, Payline_buts[i].image.color.g, Payline_buts[i].image.color.b, 1f);
                //Payline_buts[i].interactable = true;

            }
        }
    }


    //Implementing win line numbers highlight
    public void reset_win_but()
    {
        for (int i = 0; i < Payline_buts.Length; i++)
        {
            Payline_buts[i].GetComponent<Image>().sprite = Resources.Load<Sprite>("Level_Design/Buttons/Unselected/" + lvl_theme.ToString());
            Payline_buts[i].gameObject.transform.GetComponentInChildren<TextMeshProUGUI>().fontSize = 25;
        }
    }

    public void disable_winline_but()
    {
        for (int i = 0; i < Payline_buts.Length; i++)
        {
            Payline_buts[i].enabled = false;
        }

    }
    public void enable_winline_but()
    {
        for (int i = 0; i < Payline_buts.Length; i++)
        {
            Payline_buts[i].enabled = true;
        }

    }

    public void highlight_win_but(int i)
    {
        Payline_buts[i].GetComponent<Image>().sprite = Resources.Load<Sprite>("Level_Design/Buttons/Selected/" + lvl_theme.ToString());
    }

    public void updateUI()
    {
        updateBet();
        updateWon();
        updateCredits();
        updateLines();
        //updateLevel();
        updatebetperline();
        updateScatters();
    }
    void updateBet()
    {
        bet.text = AbbrevationUtility.AbbreviateNumber(slot.refs.credits.totalBet()).ToString();
    }
    void updateWon()
    {
        won.text = AbbrevationUtility.AbbreviateNumber(slot.refs.credits.lastWin).ToString();
    }
    void updateCredits()
    {
        credits.text = slot.refs.credits.totalCreditsReadout().ToString("#,0", nfi);
        Gamemanager.Instance.credits.text = credits.text;
        //changing coins
    }
    void updateLines()
    {
        lines.text = slot.refs.credits.linesPlayed.ToString();
    }
    void updatebetperline()
    {
        betperline.text = AbbrevationUtility.AbbreviateNumber(slot.refs.credits.betPerLine).ToString();
    }
    public void updateLevel()
    {

        leveling.lvl_up_check(leveling.GetLevel());
        level.text = leveling.GetLevel().ToString();
        Gamemanager.Instance.level.text = level.text;

    }

    public void Start_update_level()
    {
        level.text = leveling.GetLevel().ToString();
        Gamemanager.Instance.level.text = level.text;
    }
    void updateScatters()
    {
        scatter.text = scatters.GetScatter().ToString("#,0", nfi);

    }


    public void fiveofakind()
    {
        Fiveofkindison = true;
        fiveofkind_panel.SetActive(true);
        //print("% of a kind");
        if (temp_5ok == null)
        {
            temp_5ok = Instantiate(ob_5OK, Vector3.zero, Quaternion.identity);
            temp_5ok.transform.parent = slot.gameObject.transform.parent.transform;
                //Debug.Log("called"+ Audio_manager.Instance.slot_sound.clip);
            //Debug.Log("called" + Audio_manager.Instance.slot_sound.clip);
            Audio_manager.Instance.slot_sound.clip = Audio_manager.Instance.fiveOK;
            Audio_manager.Instance.slot_sound.volume = 0.4f;
            Audio_manager.Instance.slot_win.Stop();
            Audio_manager.Instance.slot_sound.Play();
            StartCoroutine(ExecuteAfterTime(4f, () =>
            {
                Audio_manager.Instance.slot_sound.volume = 0.53f;
                Audio_manager.Instance.slot_sound.clip = Audio_manager.Instance.spin;
                Fiveofkindison = false;
                fiveofkind_panel.SetActive(false);
                Destroy(temp_5ok);

            }));
        }
        //updateLevel();
    }





    public void LEvel_UP(int a)
    {
        Time.timeScale = 0;
        //print("time0");
        Lvl_icon_level_up_panel.sprite = Resources.Load<Sprite>("Level_Design/Wild_icons/" + (int.Parse(level.text) + 1).ToString());
        lvl_up_panel.SetActive(true);
        old_lvl = a;
        lvl_up_lvl_value.text = (int.Parse(level.text) + 1).ToString();
        lvl_up_reward.text = (old_lvl * Level_bonusMultiplier).ToString() + " COINS";
        lvl_up_panel.SetActive(true);
        Gamemanager.Instance.onlvl_up_panelon();

    }

    public void notenough_money_Function()
    {
        Not_enough_Money_Panel.SetActive(true);
    }
    public void not_enough_buy_now()
    {
        Not_enough_Money_Panel.SetActive(false);
        Gamemanager.Instance.Shop_panel.SetActive(true);
    }


    public void Collect_level_up_reward()
    {
        Time.timeScale = 1;
        lvl_up_panel.SetActive(false);
        Gamemanager.Instance.off_lvl_up_panel();
        //Gameplay_panel.SetActive(false);
        //Gamemanager.Instance.home_panel.SetActive(true);
        Gamemanager.Instance.save_scattersvalue();
        Gamemanager.Instance.ads_controller.GetComponent<Ads_priority_script>().Show_interrestial();
        collect_after_lobby();
        //GetComponent<Spin_coin_script>().on_lvl_up_collect();
        Gamemanager.Instance.GetComponent<Coin_effect_home_panel>().on_buy_coin();
        
        Invoke("waitfor_coin_effect", 1f);

    }

    public void collect_after_lobby()
    {
        Destroy(lvl_theme_container_holder);
        scatters.SaveScatters();

        Gamemanager.Instance.Update_Locked_theme_lvl();
        Gamemanager.Instance.highlight_lvl_but(old_lvl);
        // bundleloadrequest.
        Gamemanager.Instance.home_panel.SetActive(true);
        Gamemanager.Instance.Gameplay_panel.SetActive(false);
        slot.refs.credits.save_custom();

        //if (lvl_theme == 5)
        //{
        GetComponent<Level_Effects>().hide_particle();
    }

    public void waitfor_coin_effect()
    {
        //slot.refs.credits.depositCredits(old_lvl * Level_bonusMultiplier);
        PlayerPrefs.SetString("Slot_credits", (long.Parse(PlayerPrefs.GetString("Slot_credits")) + (old_lvl * Level_bonusMultiplier)).ToString());
        //Debug.Log(old_lvl * Level_bonusMultiplier);
        Gamemanager.Instance.GetComponent<Coin_effect_home_panel>().credits_value = old_lvl * Level_bonusMultiplier;
        if (totalwon_lvl>0)
        {
            Gamemanager.Instance.GetComponent<Coin_effect_home_panel>().on_buy_coin();
            Gamemanager.Instance.GetComponent<Coin_effect_home_panel>().credits_value = totalwon_lvl;
            Invoke("home_panel_coin_wait", 1f);
        }
        else
        {
            Update_API_data.Instance.is_update = 1;
            Update_API_data.Instance.type = "casino";
            Update_API_data.Instance.Update_data();
        }
        updateLevel();
        updateUI();
        
    }
    public void home_panel_coin_wait()
    {
        PlayerPrefs.SetString("Slot_credits", (long.Parse(PlayerPrefs.GetString("Slot_credits") )+ totalwon_lvl).ToString());
        Update_API_data.Instance.is_update = 1;
        Update_API_data.Instance.type = "casino";
        Update_API_data.Instance.Update_data();
    }

    IEnumerator ExecuteAfterTime(float time, Action task)
    {
        if (isCoroutineExecuting)
            yield break;
        isCoroutineExecuting = true;
        yield return new WaitForSeconds(time);
        task();
        isCoroutineExecuting = false;
    }


    public void Lobby_click()
    {


        scatters.SaveScatters();

        Gamemanager.Instance.Update_Locked_theme_lvl();
        if (slot.state == SlotState.spinning || slot.state == SlotState.snapping || holdspinscript.Instance.autostart)
        {
            holdspinscript.Instance.OnpointerClick();
            StartCoroutine(waitforframe());
        }

        else
        {
            Gamemanager.Instance.save_scattersvalue();
            Destroy(lvl_theme_container_holder);
            // bundleloadrequest.
            Gamemanager.Instance.home_panel.SetActive(true);
            Gamemanager.Instance.Gameplay_panel.SetActive(false);
            slot.refs.credits.save_custom();

            //if (lvl_theme == 5)
            //{
            GetComponent<Level_Effects>().hide_particle();
            //}
        }
        Gamemanager.Instance.ads_controller.GetComponent<Ads_priority_script>().Show_interrestial();
        Update_API_data.Instance.is_update = 1;
        Update_API_data.Instance.type = "casino";
        Update_API_data.Instance.Update_data();
    }

    public void Jackpot_win()
    {
        Time.timeScale = 0;
        Jackpot_value_incrementor();
        Jackpot_panel.SetActive(true);
        Gamemanager.Instance.Bg_shines.gameObject.SetActive(true);
        Gamemanager.Instance.Bg_shines.Play();
        Gamemanager.Instance.Coins_particle.gameObject.SetActive(true);
        Gamemanager.Instance.Coins_particle.Play();


    }


    public void Jackpot_value_incrementor()
    {
        target_coins = Gamemanager.Instance.scatter_default_value[lvl_theme - 1];
        current_coins = 0;
        //target_coins += current_coins;
    }

    public void Jackpot_Collect()
    {
        Time.timeScale = 1;
        Jackpot_panel.SetActive(false);
        //PlayerPrefs.SetString("Slot_credits", );
        Gamemanager.Instance.Bg_shines.gameObject.SetActive(false);
        Gamemanager.Instance.Coins_particle.gameObject.SetActive(false);
        updateUI();

    }


    IEnumerator waitforframe()
    {
        yield return new WaitForSeconds(0.5f);
        Gamemanager.Instance.save_scattersvalue();
        Destroy(lvl_theme_container_holder);
        //bundleloadrequest.Unload(true);
        Gamemanager.Instance.home_panel.SetActive(true);
        Gamemanager.Instance.Gameplay_panel.SetActive(false);
        slot.refs.credits.save_custom();
        GetComponent<Level_Effects>().hide_particle();
    }

    public void Menu_panel_on()
    {
        Time.timeScale = 0;
    }

    public void Menu_panel_close()
    {
        Time.timeScale = 1;
    }




   public void OnPurchase_coins(long amount)
    {
        // slot.refs.credits.credits = long.Parse(PlayerPrefs.GetString("Slot_credits", "500000"));
        temp_amont = amount;
        Debug.Log("crazy");
        GetComponent<Spin_coin_script>().on_buy_coin();
        Invoke("waitfor_coin_effect_onpurchase", 1f);
    }

    public void waitfor_coin_effect_onpurchase()
    {
        slot.refs.credits.depositCredits(temp_amont);
        updateUI();
    }
}
