﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using AppleAuth;
using AppleAuth.Enums;
using AppleAuth.Extensions;
using AppleAuth.Interfaces;
using AppleAuth.Native;
using Firebase.Auth;
using Firebase.Extensions;

using UnityEngine;
using UnityEngine.UI;

public class apple_signin : MonoBehaviour
{
    private const string AppleUserIdKey = "AppleUserId";
    private IAppleAuthManager _appleAuthManager;

    private FirebaseAuth firebaseAuth;
    FirebaseUser user_fire;

    private void Start()
    {
        // If the current platform is supported
        if (AppleAuthManager.IsCurrentPlatformSupported)
        {
            // Creates a default JSON deserializer, to transform JSON Native responses to C# instances
            var deserializer = new PayloadDeserializer();
            // Creates an Apple Authentication manager with the deserializer
           _appleAuthManager = new AppleAuthManager(deserializer);
        }

        //this.InitializeLoginMenu();
    }

    private void Update()
    {
        // Updates the AppleAuthManager instance to execute
        // pending callbacks inside Unity's execution loop
        if (this._appleAuthManager != null)
        {
            this._appleAuthManager.Update();
        }

        //this.LoginMenu.UpdateLoadingMessage(Time.deltaTime);
    }

    public void SignInWithAppleButtonPressed()
    {
        AttemptQuickLogin();
        //this.SetupLoginMenuForAppleSignIn();
        //SignInWithApple();
        //PerformLoginWithAppleIdAndFirebase ;
        //this.PerformLoginWithAppleIdAndFirebase(newmethod());
    }

    //private Action<FirebaseUser> newmethod()
    //{
    //    throw new NotImplementedException();
    //}

    private void SignInWithApple()
    {
        var loginArgs = new AppleAuthLoginArgs(LoginOptions.IncludeEmail | LoginOptions.IncludeFullName);
        Debug.Log("heare::"+loginArgs);
        Debug.Log(_appleAuthManager);
        _appleAuthManager.LoginWithAppleId(
            loginArgs,
            credential =>
            {
                // If a sign in with apple succeeds, we should have obtained the credential with the user id, name, and email, save it
                PlayerPrefs.SetString(AppleUserIdKey, credential.User);
                //this.SetupGameMenu(credential.User, credential);
                ICredential _credential = credential;
                var appleIdCredential = _credential as IAppleIDCredential;
                //Send_Data_to_SErver.
                //appleIdCredential.
                Send_Data_to_SErver.Instance.login_type = "apple";
                Send_Data_to_SErver.Instance.fb_id = appleIdCredential.User;
                //Debug.Log("Send_Data_to_SErver.Instance.fb_id" + Send_Data_to_SErver.Instance.fb_id);
                Send_Data_to_SErver.Instance.email = appleIdCredential.Email;
                //Debug.Log("Send_Data_to_SErver.Instance.email" + Send_Data_to_SErver.Instance.email);
                string temp = appleIdCredential.FullName.ToString();
                Debug.Log("Apple_name:: "+temp);
                Send_Data_to_SErver.Instance.Name = appleIdCredential.FullName.ToString().Substring(0, 9);
                Guest_login_script.Instance.Save_image_to_file(Guest_login_script.Instance.temp_tex);
                //Debug.Log("Send_Data_to_SErver.Instance.Name" + Send_Data_to_SErver.Instance.Name);
                //url = appleIdCredential.;
                //Debug.Log("url" + url);
                //Send_Data_to_SErver.Instance.profile_picture_link = url.ToString();
                if (Splash_Panel_scripts.Instance.gameObject.activeSelf)
                {
                    //set_data();
                    Splash_Panel_scripts.Instance.Loading_screen();
                }
                Send_Data_to_SErver.Instance.Upload_data();
                SIgn_In_done();
            },
            error =>
            {
                var authorizationErrorCode = error.GetAuthorizationErrorCode();
                Debug.LogWarning("Sign in with Apple failed " + authorizationErrorCode.ToString() + " " + error.ToString());
                //this.SetupLoginMenuForSignInWithApple();
            });
    }

    private void AttemptQuickLogin()
    {
        var quickLoginArgs = new AppleAuthQuickLoginArgs();

        // Quick login should succeed if the credential was authorized before and not revoked
        this._appleAuthManager.QuickLogin(
            quickLoginArgs,
            credential =>
            {
                // If it's an Apple credential, save the user ID, for later logins
                var appleIdCredential = credential as IAppleIDCredential;
                if (appleIdCredential != null)
                {
                    PlayerPrefs.SetString(AppleUserIdKey, credential.User);
                }
                Send_Data_to_SErver.Instance.login_type = "apple";
                Send_Data_to_SErver.Instance.fb_id = "user";
                //Debug.Log("Send_Data_to_SErver.Instance.fb_id" + Send_Data_to_SErver.Instance.fb_id);
                Send_Data_to_SErver.Instance.email = "email";
                //Debug.Log("Send_Data_to_SErver.Instance.email" + Send_Data_to_SErver.Instance.email);
                //string temp = appleIdCredential.FullName.ToString();
                //Debug.Log("Apple_name:: " + temp);
                Send_Data_to_SErver.Instance.Name = "name";
                Guest_login_script.Instance.Save_image_to_file(Guest_login_script.Instance.temp_tex);
                //Debug.Log("Send_Data_to_SErver.Instance.Name" + Send_Data_to_SErver.Instance.Name);
                //url = appleIdCredential.;
                //Debug.Log("url" + url);
                //Send_Data_to_SErver.Instance.profile_picture_link = url.ToString();
                if (Splash_Panel_scripts.Instance.gameObject.activeSelf)
                {
                    //set_data();
                    Splash_Panel_scripts.Instance.Loading_screen();
                }
                Send_Data_to_SErver.Instance.Upload_data();
                SIgn_In_done();
                //this.SetupGameMenu(credential.User, credential);
            },
            error =>
            {
                // If Quick Login fails, we should show the normal sign in with apple menu, to allow for a normal Sign In with apple
                var authorizationErrorCode = error.GetAuthorizationErrorCode();
                Debug.LogWarning("Quick Login Failed " + authorizationErrorCode.ToString() + " " + error.ToString());
                //this.SetupLoginMenuForSignInWithApple();
                if (authorizationErrorCode.ToString() != "Canceled")
                {
                    SignInWithApple();
                }
            });
    }


   public void SIgn_In_done()
    {
        Google_signin.Instance.login_but.interactable = false;
        if (PlayerPrefs.GetInt("Google_Login") == 0)
        {
            PlayerPrefs.SetInt("Google_Login", 1);
        }
        
        
    }
}
