﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Share_scripts : MonoBehaviour
{

    NativeShare ac;
    // Start is called before the first frame update
    void Start()
    {
        ac = new NativeShare();
    }

    // Update is called once per frame
    void Update()
    {
        
    }


    public void On_allshare()
    {

        print(Get_API_Data_IAP.Instance.share_private_link_message);
#if UNITY_ANDROID
        ac.SetText(Get_API_Data_IAP.Instance.share_private_link_message+"\n"+Get_API_Data_IAP.Instance.android_app_link).Share();
#elif UNITY_IOS
        ac.SetText(Get_API_Data_IAP.Instance.share_private_link_message+"\n"+Get_API_Data_IAP.Instance.ios_app_link).Share();
#endif


    }

    public void Rate_us()
    {
#if UNITY_ANDROID
        Application.OpenURL(Get_API_Data_IAP.Instance.android_app_link);
#elif UNITY_IOS
       Application.OpenURL(Get_API_Data_IAP.Instance.ios_app_link);
#endif

    }


}
