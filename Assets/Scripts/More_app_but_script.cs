﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class More_app_but_script : MonoBehaviour
{
    public Image icon;
    public TextMeshProUGUI Game_name;
    public Button install;
    public string app_link;
    public string image_url;

    // Start is called before the first frame update
    void Start()
    {
        Get_image();
        Add_onclick_event();
    }


    //private void OnEnable()
    //{
    //    Get_image();
    //}
    // Update is called once per frame
    void Update()
    {
        
    }

    public void Install_onclick()
    {
        Audio_manager.Instance.Simple_button_click();
        Application.OpenURL(app_link);
    }

    public void Get_image()
    {
        StartCoroutine("downloadImg", image_url);
    }

    IEnumerator downloadImg(string url)
    {
        Texture2D texture = new Texture2D(1, 1);
        WWW www = new WWW(url);
        yield return www;
        www.LoadImageIntoTexture(texture);

        Sprite image = Sprite.Create(texture, new Rect(0, 0, texture.width, texture.height), new Vector2(0.5f, 0.5f));
       icon.sprite = image;
    }

    public void Add_onclick_event()
    {
        install.onClick.AddListener(() => open_url());
    }

    public void open_url()
    {
        Application.OpenURL(app_link);
    }
}
