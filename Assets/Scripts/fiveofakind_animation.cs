﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEditor;


public class fiveofakind_animation : MonoBehaviour
{


    //public particle_handleer_5OK PH;
    public enum enMenuScreen
    { left, right, top, bot };


    public enMenuScreen enCurrent = enMenuScreen.left;

    public int speed;
    public Transform destination;
    public bool done = true;
    //public bool enable_ob = false;
    // Start is called before the first frame update
    void Start()
    {

    }

    private void OnEnable()
    {
        //enable_ob = true;
    }
    // Update is called once per frame
    void Update()
    {
        //if (enable_ob)
        //{
            float step = speed * Time.deltaTime;
            transform.position = Vector3.MoveTowards(transform.position, destination.position, step);

            if (transform.position == destination.position)
            {
                //done = true
                if (done)
                {
                    switch (enCurrent)
                    {
                        case enMenuScreen.left:
                            transform.DOPunchScale(new Vector3(0.1f, 0.1f, 0), 1, 0);
                            done = false;
                            break;
                        case enMenuScreen.right:
                        transform.DOPunchScale(new Vector3(0.1f, 0.1f, 0), 1, 0);
                        //transform.DOPunchPosition(new Vector3(-0.3f, 0, 0), 1, 0);
                            done = false;
                            break;
                        case enMenuScreen.bot:
                        transform.DOPunchScale(new Vector3(0.1f, 0.1f, 0), 1, 0);
                        //transform.DOPunchPosition(new Vector3(0, 0.3f, 0), 1, 0);
                            done = false;
                            break;
                        case enMenuScreen.top:
                        transform.DOPunchScale(new Vector3(0.1f, 0.1f, 0), 1, 0);
                        //transform.DOPunchPosition(new Vector3(0, -0.3f, 0), 1, 0);
                            done = false;
                            break;
                    }
                    done = false;
                }

            }
        //}

    }
}
