﻿using UnityEngine.Events;
using UnityEngine;
using GoogleMobileAds.Api;
using GoogleMobileAds.Common;
using UnityEngine.UI;
using System;
using System.Collections.Generic;

public class AdsController : MonoBehaviour
{
    //private BannerView bannerView;
    private InterstitialAd interstitialAd;
    private RewardedAd rewardedAd;
    //private RewardedInterstitialAd rewardedInterstitialAd;
    //private float deltaTime;
    public GameObject admob_bg;
    public GameObject daily_Loading_panel;
    public GameObject daily_noice_panel;
    public UnityEvent OnAdLoadedEvent;
    public UnityEvent OnAdFailedToLoadEvent;
    public UnityEvent OnAdOpeningEvent;
    public UnityEvent OnAdFailedToShowEvent;
    public UnityEvent OnUserEarnedRewardEvent;
    public UnityEvent OnAdClosedEvent;
    public UnityEvent OnAdLeavingApplicationEvent;
   

    public bool Daily_reward = false;

    public string admob_interstitial_id;
    public string admob_reward_id;

    #region UNITY MONOBEHAVIOR METHODS

    public void Manual_Start()
    {
        //get ids from server
#if UNITY_ANDROID
        admob_interstitial_id = Ads_initialize_API.Instance.android_admob_interstitial_id;
        admob_reward_id = Ads_initialize_API.Instance.android_admob_reward_id;
#elif UNITY_IOS
        admob_interstitial_id = Ads_initialize_API.Instance.ios_admob_interstitial_id;
        admob_reward_id = Ads_initialize_API.Instance.ios_admob_reward_id;
#endif
//        MobileAds.SetiOSAppPauseOnBackground(true);

//        List<String> deviceIds = new List<String>() { AdRequest.TestDeviceSimulator };

//        // Add some test device IDs (replace with your own device IDs).
//#if UNITY_IOS
//        deviceIds.Add("96e23e80653bb28980d3f40beb58915c");
//#elif UNITY_ANDROID
//        deviceIds.Add("75EF8D155528C04DACBBA6F36F433035");
//#endif

//        // Configure TagForChildDirectedTreatment and test device IDs.
//        RequestConfiguration requestConfiguration =
//            new RequestConfiguration.Builder()
//            .SetTagForChildDirectedTreatment(TagForChildDirectedTreatment.Unspecified)
//            .SetTestDeviceIds(deviceIds).build();

//        MobileAds.SetRequestConfiguration(requestConfiguration);

//        // Initialize the Google Mobile Ads SDK.
//        MobileAds.Initialize(HandleInitCompleteAction);
    //}

    //private void HandleInitCompleteAction(InitializationStatus initstatus)
    //{
    //    // Callbacks from GoogleMobileAds are not guaranteed to be called on
    //    // main thread.
    //    // In this example we use MobileAdsEventExecutor to schedule these calls on
    //    // the next Update() loop.
    //    MobileAdsEventExecutor.ExecuteInUpdate(() =>
    //    {
    //        //statusText.text = "Initialization complete";
    //        //RequestBannerAd();
            //Debug.Log("Initialize done...");
            if (admob_interstitial_id != null)
            {
                RequestAndLoadInterstitialAd();
            }
            if (admob_reward_id != null)
            {
                RequestAndLoadRewardedAd();

            }
        //});
    }

    private void Update()
    {

    }

#endregion

    //#region HELPER METHODS

    //private AdRequest CreateAdRequest()
    //{
    //    return new AdRequest.Builder()
    //        .AddTestDevice(AdRequest.TestDeviceSimulator)
    //        .AddTestDevice("0123456789ABCDEF0123456789ABCDEF")
    //        .AddKeyword("unity-admob-sample")
    //        .TagForChildDirectedTreatment(false)
    //        .AddExtra("color_bg", "9B30FF")
    //        .Build();
    //}

    //#endregion

   

#region INTERSTITIAL ADS

    public void RequestAndLoadInterstitialAd()
    {
        //statusText.text = "Requesting Interstitial Ad.";
#if UNITY_EDITOR
        string adUnitId = "unused";
#elif UNITY_ANDROID
        string adUnitId = admob_interstitial_id;
#elif UNITY_IOS
        string adUnitId = admob_interstitial_id;
#else
        string adUnitId = "unexpected_platform";
#endif

        // Clean up interstitial before using it
        if (interstitialAd != null)
        {
            interstitialAd.Destroy();
        }

        interstitialAd = new InterstitialAd(adUnitId);

        // Add Event Handlers
        interstitialAd.OnAdLoaded += (sender, args) => OnAdLoadedEvent.Invoke();
        interstitialAd.OnAdFailedToLoad += (sender, args) => OnAdFailedToLoadEvent.Invoke();
        interstitialAd.OnAdOpening += (sender, args) => OnAdOpeningEvent.Invoke();
        interstitialAd.OnAdClosed += (sender, args) => OnAdClosedEvent.Invoke();
        interstitialAd.OnAdLeavingApplication += (sender, args) => OnAdLeavingApplicationEvent.Invoke();


        AdRequest request = new AdRequest.Builder().Build();


        //// Load an interstitial ad
        interstitialAd.LoadAd(request);
        //AdRequest request = new AdRequest.Builder().Build();
        //// Load the interstitial with the request.
        //interstitialAd.LoadAd(request);
    }


    public bool check_loaded_InterstitialAd()
    {
        Debug.Log(interstitialAd.IsLoaded());
        
        return interstitialAd.IsLoaded();
    }
    public void ShowInterstitialAd()
    {

        admob_bg.SetActive(true);
            interstitialAd.Show();

    }

    public void DestroyInterstitialAd()
    {
        if (interstitialAd != null)
        {
            interstitialAd.Destroy();
        }
    }
#endregion

#region REWARDED ADS

    public void RequestAndLoadRewardedAd()
    {
        //statusText.text = "Requesting Rewarded Ad.";
#if UNITY_EDITOR
        string adUnitId = "unused";
#elif UNITY_ANDROID
        string adUnitId = admob_reward_id;
#elif UNITY_IOS
        string adUnitId = admob_reward_id;
#else
        string adUnitId = "unexpected_platform";
#endif

        // create new rewarded ad instance
        rewardedAd = new RewardedAd(adUnitId);

        // Add Event Handlers
        rewardedAd.OnAdLoaded += (sender, args) => OnAdLoadedEvent.Invoke();
        rewardedAd.OnAdFailedToLoad += (sender, args) => OnAdFailedToLoadEvent.Invoke();
        rewardedAd.OnAdOpening += (sender, args) => OnAdOpeningEvent.Invoke();
        rewardedAd.OnAdFailedToShow += (sender, args) => OnAdFailedToShowEvent.Invoke();
        rewardedAd.OnAdClosed += (sender, args) => OnAdClosedEvent.Invoke();
        rewardedAd.OnUserEarnedReward += (sender, args) => OnUserEarnedRewardEvent.Invoke();
        AdRequest request = new AdRequest.Builder().Build();
        // Create empty ad request
        rewardedAd.LoadAd(request);
        //AdRequest request = new AdRequest.Builder().Build();
        //// Load the rewarded ad with the request.
        //rewardedAd.LoadAd(request);
    }


    public bool check_loaded_rewarded()
    {
        return rewardedAd.IsLoaded();
    }
    public void ShowRewardedAd()
    {

        if (rewardedAd != null)
        {
            admob_bg.SetActive(true);
            rewardedAd.Show();
        }
        else
        {
            //statusText.text = "Rewarded ad is not ready yet.";
        }
    }

    public void Reward_money_Add()
    {
        if (Daily_reward)
        {
            Gamemanager.Instance.transform.GetComponent<DailyReward>().open_chest_after_ads();
            //PlayerPrefs.SetString("Slot_credits", (long.Parse(PlayerPrefs.GetString("Slot_credits")) + Gamemanager.Instance.transform.GetComponent<DailyReward>().add_coins).ToString());
            //Gamemanager.Instance.Setting_vlaues();
            ////Gamemanager.Instance.transform.GetComponent<DailyReward>().CLose_bonus_panel_();
            Daily_reward = false;
        }
        else
        {

            Gamemanager.Instance.On_Rewarded_money_add(Get_API_Data_IAP.Instance.watch_video_coin);
        }
    }












#endregion


    public void REquest_ads()
    {
        Invoke("waitted_req", 0.5f);

    }


    public void waitted_req()
    {
        if (admob_interstitial_id != null)
        {
            if (!check_loaded_InterstitialAd())
            {

                RequestAndLoadInterstitialAd();

            }
        }
        if (admob_reward_id != null)
        {

            if (!check_loaded_rewarded())
            {
                RequestAndLoadRewardedAd();
            }
        }
        //if (Ads_initialize_API.Instance.android_facebook_interstitial_id != null)
        //{
        //    if (!transform.GetComponent<FB_ADS_script>().Check_interstatial_fb_ads())
        //    {
        //        transform.GetComponent<FB_ADS_script>().LoadInterstitial();
        //    }
        //}
        //if (Ads_initialize_API.Instance.android_facebook_reward_id != null)
        //{
        //    if (!transform.GetComponent<FB_ADS_script>().Check_rewarded_fb_ads())
        //    {
        //        transform.GetComponent<FB_ADS_script>().LoadRewardedVideo();
        //    }
        //}

    }
    public void Disable_admob_bg()
    {
        admob_bg.SetActive(false);
    }

    public void Daily_bonus_ads_cancled()
    {
        Invoke("cancle_popup_shower", 0.5f);
        //if (Daily_reward)
        //{
        //    if (!daily_noice_panel.activeSelf)
        //    {//show cancle ad panel
        //        daily_noice_panel.SetActive(true);
        //        //Invoke("disable_daily", 5f);
        //    }
        //}
    }


    public void cancle_popup_shower()
    {
        if (Daily_reward)
        {
            if (!daily_noice_panel.activeSelf)
            {//show cancle ad panel
                daily_noice_panel.SetActive(true);
                Invoke("disable_daily", 5f);
            }
        }
    }


    public void Daily_bonus_ads_loading_screen()
    {
        if (!daily_Loading_panel.activeSelf)
        {
            daily_Loading_panel.SetActive(true);
            Invoke("disable_daily", 5f);
        }
    }

    public void disable_daily()
    {
        daily_Loading_panel.SetActive(false);
        daily_noice_panel.SetActive(false);
    }
}