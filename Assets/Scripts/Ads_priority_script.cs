﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ads_priority_script : MonoBehaviour
{

    public GameObject popup_watchads;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }



    public void Show_interrestial()
    {
        if (Ads_initialize_API.Instance.interstitial_ads_priority == "admob")
        {
            if (transform.GetComponent<AdsController>().check_loaded_InterstitialAd())
            {
                Debug.Log("Inadmob_true");
                transform.GetComponent<AdsController>().ShowInterstitialAd();
            }
            else if (transform.GetComponent<FB_ADS_script>().Check_interstatial_fb_ads())
            {
                transform.GetComponent<FB_ADS_script>().ShowInterstitial();
            }
            //Debug.Log("after admob");
            else if (transform.GetComponent<Unity_Ads_script>().check_unity_interstatial())
            {
                Debug.Log("IN_Unity_true");
                transform.GetComponent<Unity_Ads_script>().ShowInterstitialAd();
            }
            
        }
        else if (Ads_initialize_API.Instance.interstitial_ads_priority == "fb")
        {
            if (transform.GetComponent<FB_ADS_script>().Check_interstatial_fb_ads())
            {
                transform.GetComponent<FB_ADS_script>().ShowInterstitial();
            }
            else if (transform.GetComponent<Unity_Ads_script>().check_unity_interstatial())
            {
                transform.GetComponent<Unity_Ads_script>().ShowInterstitialAd();
            }
            else if (transform.GetComponent<AdsController>().check_loaded_InterstitialAd())
            {
                transform.GetComponent<AdsController>().ShowInterstitialAd();
            }
           
        }
        else if (Ads_initialize_API.Instance.interstitial_ads_priority == "unity")
        {
            if (transform.GetComponent<Unity_Ads_script>().check_unity_interstatial())
            {
                transform.GetComponent<Unity_Ads_script>().ShowInterstitialAd();
            }
            else if (transform.GetComponent<FB_ADS_script>().Check_interstatial_fb_ads())
            {
                transform.GetComponent<FB_ADS_script>().ShowInterstitial();
            }
            else if (transform.GetComponent<AdsController>().check_loaded_InterstitialAd())
            {
                transform.GetComponent<AdsController>().ShowInterstitialAd();
            }
            
        }

    }

    public void Show_RewardedAds()
    {
        if (Ads_initialize_API.Instance.ads_priority == "admob")
        {
            if (transform.GetComponent<AdsController>().check_loaded_rewarded())
            {
                transform.GetComponent<AdsController>().ShowRewardedAd();
            }
            else if (transform.GetComponent<FB_ADS_script>().Check_rewarded_fb_ads())
            {
                transform.GetComponent<FB_ADS_script>().ShowRewardedVideo();
            }
            else if (transform.GetComponent<Unity_Ads_script>().check_unity_rewarded())
            {
                transform.GetComponent<Unity_Ads_script>().ShowRewardedVideo();
            }
            else
            {
                show_popup();
            }
        }
        else if (Ads_initialize_API.Instance.ads_priority == "fb")
        {
            if (transform.GetComponent<FB_ADS_script>().Check_rewarded_fb_ads())
            {
                transform.GetComponent<FB_ADS_script>().ShowRewardedVideo();
            }
            else if (transform.GetComponent<Unity_Ads_script>().check_unity_rewarded())
            {
                transform.GetComponent<Unity_Ads_script>().ShowRewardedVideo();
            }
            else if (transform.GetComponent<AdsController>().check_loaded_rewarded())
            {
                transform.GetComponent<AdsController>().ShowRewardedAd();
            }
            else
            {
                show_popup();
            }
            
        }
        else if (Ads_initialize_API.Instance.ads_priority == "unity")
        {
            if (transform.GetComponent<Unity_Ads_script>().check_unity_rewarded())
            {
                transform.GetComponent<Unity_Ads_script>().ShowRewardedVideo();
            }
            else if (transform.GetComponent<FB_ADS_script>().Check_rewarded_fb_ads())
            {
                transform.GetComponent<FB_ADS_script>().ShowRewardedVideo();
            }
            else if (transform.GetComponent<AdsController>().check_loaded_rewarded())
            {
                transform.GetComponent<AdsController>().ShowRewardedAd();
            }
            else
            {
                show_popup();
            }
        }
    }

    public void show_popup()
    {
        if (GetComponent<AdsController>().Daily_reward)
        {
            GetComponent<AdsController>().Daily_bonus_ads_loading_screen();
        }
        else
        {
            if (!popup_watchads.activeSelf)
            {
                popup_watchads.SetActive(true);
                Invoke("hide_popup", 2f);
            }
        }
        
    }

    public void hide_popup()
    {
        popup_watchads.SetActive(false);
    }
}
