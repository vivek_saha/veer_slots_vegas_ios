﻿using UnityEngine;
using AudienceNetwork;


public class FB_ADS_script : MonoBehaviour
{

    private InterstitialAd interstitialAd;
    private RewardedVideoAd rewardedVideoAd;
    public string facebook_interstitial_id;
    public string facebook_reward_id;
    private bool isLoaded_interstatial;
    private bool isLoaded_rewarded;
    private bool didClose;


    
    public void Manual_Start()
    {
#if UNITY_IOS
        facebook_interstitial_id = Ads_initialize_API.Instance.ios_facebook_interstitial_id;
        facebook_reward_id = Ads_initialize_API.Instance.ios_facebook_reward_id;
#elif UNITY_ANDROID
        facebook_interstitial_id = Ads_initialize_API.Instance.android_facebook_interstitial_id;
        facebook_reward_id = Ads_initialize_API.Instance.android_facebook_reward_id;
#endif
        AudienceNetworkAds.Initialize();


        Invoke("wait_for_fb_initialize", 1f);
    }


    public void wait_for_fb_initialize()
    {
        if (facebook_interstitial_id != null)
        {
            LoadInterstitial();
        }
        if (facebook_reward_id != null)
        {
            LoadRewardedVideo();
        }

    }
    public void LoadInterstitial()
    {
        
        interstitialAd = new InterstitialAd(facebook_interstitial_id);

        interstitialAd.Register(gameObject);

        // Set delegates to get notified on changes or when the user interacts with the ad.
        interstitialAd.InterstitialAdDidLoad = delegate ()
        {
            Debug.Log("Interstitial ad loaded.");
            isLoaded_interstatial = true;
            didClose = false;
            string isAdValid = interstitialAd.IsValid() ? "valid" : "invalid";
           
        };
        interstitialAd.InterstitialAdDidFailWithError = delegate (string error)
        {
            Debug.Log("Interstitial ad failed to load with error: " + error);
            
        };
        interstitialAd.InterstitialAdWillLogImpression = delegate ()
        {
            Debug.Log("Interstitial ad logged impression.");
        };
        interstitialAd.InterstitialAdDidClick = delegate ()
        {
            Debug.Log("Interstitial ad clicked.");
        };
        interstitialAd.InterstitialAdDidClose = delegate ()
        {
            transform.GetComponent<AdsController>().REquest_ads();
            transform.GetComponent<AdsController>().Disable_admob_bg();
            Debug.Log("Interstitial ad did close.");
            didClose = true;
            if (interstitialAd != null)
            {
                interstitialAd.Dispose();
            }
        };



        // Initiate the request to load the ad.
        interstitialAd.LoadAd();
    }


    public bool Check_interstatial_fb_ads()
    {
        return isLoaded_interstatial;
    }
    // Show button
    public void ShowInterstitial()
    {
        if (isLoaded_interstatial)
        {
            interstitialAd.Show();
            isLoaded_interstatial = false;
            //statusLabel.text = "";
        }
        else
        {
            //statusLabel.text = "Ad not loaded. Click load to request an ad.";
        }
    }

    void OnDestroy()
    {
        //Dispose of interstitial ad when the scene is destroyed
        if (interstitialAd != null)
        {
            interstitialAd.Dispose();
        }
        Debug.Log("InterstitialAdTest was destroyed!");
        //Dispose of rewardedVideo ad when the scene is destroyed
        if (rewardedVideoAd != null)
        {
            rewardedVideoAd.Dispose();
        }
        Debug.Log("RewardedVideoAdTest was destroyed!");
    }


    public void LoadRewardedVideo()
    {
        
        rewardedVideoAd = new RewardedVideoAd(facebook_reward_id);


        rewardedVideoAd.Register(gameObject);

      
        rewardedVideoAd.RewardedVideoAdDidLoad = delegate ()
        {
            Debug.Log("RewardedVideo ad loaded.");
            isLoaded_rewarded = true;
            didClose = false;
            string isAdValid = rewardedVideoAd.IsValid() ? "valid" : "invalid";
            //statusLabel.text = "Ad loaded and is " + isAdValid + ". Click show to present!";
        };
        rewardedVideoAd.RewardedVideoAdDidFailWithError = delegate (string error)
        {
            Debug.Log("RewardedVideo ad failed to load with error: " + error);
            //statusLabel.text = "RewardedVideo ad failed to load. Check console for details.";
        };
        rewardedVideoAd.RewardedVideoAdWillLogImpression = delegate ()
        {
            Debug.Log("RewardedVideo ad logged impression.");
        };
        rewardedVideoAd.RewardedVideoAdDidClick = delegate ()
        {
            Debug.Log("RewardedVideo ad clicked.");
        };

       

        rewardedVideoAd.RewardedVideoAdDidFail = delegate ()
        {
            Debug.Log("Rewarded video ad not validated, or no response from server");
        };

        rewardedVideoAd.RewardedVideoAdDidClose = delegate ()
        {
            transform.GetComponent<AdsController>().Disable_admob_bg();
            transform.GetComponent<AdsController>().Reward_money_Add();
            transform.GetComponent<AdsController>().REquest_ads();
            Debug.Log("Rewarded video ad did close.");
            didClose = true;
            if (rewardedVideoAd != null)
            {
                rewardedVideoAd.Dispose();
            }
        };

        // Initiate the request to load the ad.
        rewardedVideoAd.LoadAd();
    }

    public bool Check_rewarded_fb_ads()
    {
        return isLoaded_rewarded;
    }

    // Show button
    public void ShowRewardedVideo()
    {
        if (isLoaded_rewarded)
        {
            rewardedVideoAd.Show();
            isLoaded_rewarded = false;
            //statusLabel.text = "";
        }
        else
        {
            //statusLabel.text = "Ad not loaded. Click load to request an ad.";
        }
    }


  
}
