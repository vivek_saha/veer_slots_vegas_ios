﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.UI;

public class Splash_Panel_scripts : MonoBehaviour
{
    public static Splash_Panel_scripts Instance;
    public GameObject Main_splash_panel;
    public GameObject Notice_panel;
    public GameObject no_internet_connection_panel;
    public GameObject Something_went_wrong_panel;
    public GameObject Update_notice_Panel;
    public GameObject Loading_panel;
    public GameObject UnderMaintenance_panel;
    public GameObject Login_panel;
    public GameObject Loading_screen_obj;
    public GameObject Blocked_panel;
    public Slider splash_loading_slider;
    public float splash_load_time = 2;

    public GameObject shine_particle_splash_screen;
    public bool startup_panel_ready= false;
    //public GameObject Facebook_but;
    //public GameObject playas_a_guest;


    GameObject ac;
    private void Awake()
    {
        Instance = this;
    }
    // Start is called before the first frame update
    private void Start()
    {
        splash_loading_slider.DOValue(1f, splash_load_time);
        StartCoroutine("waitfor_main_splash");
    }


    IEnumerator waitfor_main_splash()
    {
        yield return new WaitForSeconds(2f);
        Main_splash_panel.SetActive(false);
        ac = Instantiate(shine_particle_splash_screen, new Vector3(0,2.3f,0), Quaternion.identity);
        ac.GetComponent<ParticleSystem>().Play();
        manual_Start();
    }

    public void manual_Start()
    {

        //Debug.Log(PlayerPrefs.GetString("Token", string.Empty));
        if (PlayerPrefs.GetString("Token",string.Empty)==string.Empty)
        {

            //not logged in
            Login_panel.SetActive(true);
        }
        else
        {
            //logged in
            Login_panel.SetActive(false);
            Loading_screen();
            login_type_checker();
        }
    }

    public void login_type_checker()
    {
        if (PlayerPrefs.GetInt("GuestLogin", 0) == 1)
        {
            //guest_login
        }
        else
        {
            //google_login
        }
        if (PlayerPrefs.GetInt("Google_Login") == 1)
        {
            Google_signin.Instance.login_but.interactable = false;
        }
       
        Set_User_data.Instance.Get_all_data_from_playerprefs();

    }


    public void Loading_screen()
    {
        splash_loading_slider.value = 0;
        Loading_screen_obj.SetActive(true);
        Login_panel.SetActive(false);
        
        //StartCoroutine("Loading_screen_splash");
    }


    //IEnumerator Loading_screen_splash()
    //{
    //    yield return new WaitForSeconds(splash_load_time);
    //    Loading_screen_obj.SetActive(false);
    //    if(Set_User_data.Instance.Username_server!=null)
    //    {
    //        gameObject.SetActive(false);
    //        Gamemanager.Instance.home_panel.SetActive(true);
    //    }
    //    else
    //    {
    //        Login_panel.SetActive(true);
    //    }
    //}


    public void After_load_home_screen()
    {
        Destroy(ac);
        Loading_screen_obj.SetActive(false);
        Gamemanager.Instance.home_panel.SetActive(true);
        //if(Gamemanager.Instance.startup_purchase_card_panel.GetComponent<Purchase_container_script>().Buy_value.text != string.Empty)
        //{
        //Get_API_Data_IAP.Instance.startup_purchase_card();
        //}

        if(startup_panel_ready)
        {
           Gamemanager.Instance.startup_purchase_card_panel.SetActive(true);
            startup_panel_ready = false;
        }
        gameObject.SetActive(false);

    }

    public void BLoked_fun()
    {
        Time.timeScale = 0;
        Blocked_panel.SetActive(true);

    }

    public void Close_app()
    {
        Application.Quit();
    }
    // Update is called once per frame
    void Update()
    {

    }

    //public void CLose_application()
    //{
    //    Application.Quit();
    //}


}
