﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using TMPro;
using System;
using System.IO;
using UnityEngine.UI;
using DG.Tweening;
using System.Globalization;
using IAP;

public class Gamemanager : MonoBehaviour
{
    public static Gamemanager Instance;

    [Header("IMP")]
    public int Total_level_final;
    public int App_ver_code = 1;
    public bool Unlocked_all_lvl_test = false;

    [Header("GameObject")]
    public GameObject home_panel;
    public GameObject Gameplay_panel;
    public GameObject Lvl_but_main_container;
    public GameObject lvl_up_icon;
    public GameObject Shop_panel;
    public GameObject purchaser_container;
    public GameObject Exit_game_panel;
    public GameObject splash_panel;
    public GameObject ads_controller;
    public GameObject Setting_panel;
    public GameObject Lobby_exit_panel;
    public GameObject startup_purchase_card_panel;


    [Header("Transform")]
    public Transform content_lvl_but;
    public Transform Content_Shop_panel;

    [Header("TextMeshProUGUI")]
    public TextMeshProUGUI credits;
    public TextMeshProUGUI level;

    [HideInInspector]
    [Header("lvlup ParticleSystem")]
    public ParticleSystem Bg;
    [HideInInspector]
    public ParticleSystem top_l;
    [HideInInspector]
    public ParticleSystem top_r;
    [HideInInspector]
    public ParticleSystem star;

    [HideInInspector]
    [Header("lvlup ParticleSystem")]
    public ParticleSystem Bg_shines;
    [HideInInspector]
    public ParticleSystem Coins_particle;


    [HideInInspector]
    public long[] scatter_default_value = new long[100];
    [HideInInspector]
    public string abc;
    [HideInInspector]
    public string[] array = new string[100];

    AssetBundle bundleloadrequest_tmp;

    int Ads_COunter = 1;
    public long temp_amount;

    public long coin_change_checker;
    public int xp_change_checker;



    int highligh_lvl_num = -1;
    //[Header("Script")]
    //public lvl_bar_home_panel lbh;

    NumberFormatInfo nfi;

    private void Awake()
    {
        Instance = this;


    }

    private void Start()
    {
        nfi = new CultureInfo("en-US", false).NumberFormat;
        nfi.CurrencyDecimalDigits = 0;
        //nfi = (NumberFormatInfo)CultureInfo.InvariantCulture.NumberFormat.Clone();
        nfi.NumberGroupSeparator = " ";
        //Toggle_Sound_music.Instance.Manual_Start();
        //print(Application.persistentDataPath);
    }

    public void manual_Start()
    {
        Total_level_final = Get_API_Data_IAP.Instance.Total_lvl_server;
        //Debug.Log("Application Version : " + Application.version);
        Lvl_manager.Instance.leveling.manual_start();
        Setting_vlaues();
        Generate_lvl_theme_buttons();
        Setting_vlaues();
        //print(  AbbrevationUtility.AbbreviateNumber(1000000));
        //Generate_shop_panel();
        Lvl_manager.Instance.Start_update_level();
        Splash_Panel_scripts.Instance.After_load_home_screen();
    }

    public void Generate_lvl_theme_buttons()
    {
        int count;
#if UNITY_EDITOR
        //DirectoryInfo dir = new DirectoryInfo("Assets/Resources/Level_Prefabs");
        //DirectoryInfo[] info = dir.GetDirectories("*.*");
        //count = dir.GetDirectories().Length;
        count = Total_level_final;
#elif UNITY_ANDROID
     //Debug.Log("Unity android");
     count = Total_level_final;
#else
       count = Total_level_final;
#endif


        GameObject temp_container = null;
        if (count % 2 == 0)
        {
            for (int i = 0; i < count; i++)
            {
                if (i % 2 == 0)
                {
                    temp_container = Instantiate(Lvl_but_main_container, content_lvl_but);
                    //temp_container.transform.GetChild(i%2).GetComponent<Level_Selection_script>().lvl_num = i + 1;
                }
                else
                {
                    //temp_container.transform.GetChild(1).GetComponent<Level_Selection_script>().lvl_num = i + 1;
                }
                temp_container.transform.GetChild(i % 2).GetComponent<Level_Selection_script>().main_image.GetComponent<Image>().sprite = Resources.Load<Sprite>("Level_Design/frame_image/" + (i + 1).ToString());
                temp_container.transform.GetChild(i % 2).GetComponent<Level_Selection_script>().Icon_frame.GetComponent<Image>().sprite = temp_container.transform.GetChild(i % 2).GetComponent<Level_Selection_script>().Frames[(i / 2) % 4];
                temp_container.transform.GetChild(i % 2).GetComponent<Level_Selection_script>().lvl_num = i + 1;



                //Debug.Log("Unlocked_all_lvl_test"+ Unlocked_all_lvl_test);
                if (!Unlocked_all_lvl_test)
                {
                    if (i >= PlayerPrefs.GetInt("old_lvl", 1))
                    {
                        temp_container.transform.GetChild(i % 2).GetComponent<Level_Selection_script>().locked = true;
                        temp_container.transform.GetChild(i % 2).GetComponent<Level_Selection_script>().locked_image.SetActive(true);
                        temp_container.transform.GetChild(i % 2).GetComponent<Level_Selection_script>().Lockedtext_line.SetActive(true);
                    }
                }
                if (i >= Lvl_manager.Instance.lvl_cap_server)
                {
                    if (!Check_lvl_file(i + 1))
                    {
                        temp_container.transform.GetChild(i % 2).GetComponent<Level_Selection_script>().Download_available();
                    }
                }

            }

        }
        else
        {
            for (int i = 0; i < count; i++)
            {
                if (i % 2 == 0)
                {
                    temp_container = Instantiate(Lvl_but_main_container, content_lvl_but);
                    //temp_container.transform.GetChild(i%2).GetComponent<Level_Selection_script>().lvl_num = i + 1;
                }
                else
                {
                    //temp_container.transform.GetChild(1).GetComponent<Level_Selection_script>().lvl_num = i + 1;
                }
                temp_container.transform.GetChild(i % 2).GetComponent<Level_Selection_script>().main_image.GetComponent<Image>().sprite = Resources.Load<Sprite>("Level_Design/frame_image/" + (i + 1).ToString());
                temp_container.transform.GetChild(i % 2).GetComponent<Level_Selection_script>().Icon_frame.GetComponent<Image>().sprite = temp_container.transform.GetChild(i % 2).GetComponent<Level_Selection_script>().Frames[(i / 2) % 4];
                temp_container.transform.GetChild(i % 2).GetComponent<Level_Selection_script>().lvl_num = i + 1;



                if (!Unlocked_all_lvl_test)
                {
                    if (i >= PlayerPrefs.GetInt("old_lvl", 1))
                    {
                        temp_container.transform.GetChild(i % 2).GetComponent<Level_Selection_script>().locked = true;
                        temp_container.transform.GetChild(i % 2).GetComponent<Level_Selection_script>().locked_image.SetActive(true);
                        temp_container.transform.GetChild(i % 2).GetComponent<Level_Selection_script>().Lockedtext_line.SetActive(true);
                    }
                }

                if (i == count - 1)
                {
                    temp_container.transform.GetChild(count % 2).gameObject.SetActive(false);
                }
                if (i >= Lvl_manager.Instance.lvl_cap_server)
                {
                    if (!Check_lvl_file(i + 1))
                    {
                        temp_container.transform.GetChild(i % 2).GetComponent<Level_Selection_script>().Download_available();
                    }
                }

            }

        }
    }

    public void Update_Locked_theme_lvl()
    {
        int count;
#if UNITY_EDITOR
        //DirectoryInfo dir = new DirectoryInfo("Assets/Resources/Level_Prefabs");
        //DirectoryInfo[] info = dir.GetDirectories("*.*");
        //count = dir.GetDirectories().Length;
        count = Total_level_final;
#elif UNITY_ANDROID
     //Debug.Log("Unity android");
     count = Total_level_final;
#else
       count = Total_level_final;
#endif// GameObject temp_container = null;
        if (count % 2 == 0)
        {
            for (int i = 0; i < count; i++)
            {
                if (i < PlayerPrefs.GetInt("old_lvl", 1))
                {
                    content_lvl_but.transform.GetChild(i / 2).transform.GetChild(i % 2).GetComponent<Level_Selection_script>().locked = false;
                    content_lvl_but.transform.GetChild(i / 2).transform.GetChild(i % 2).GetComponent<Level_Selection_script>().locked_image.SetActive(false);
                    content_lvl_but.transform.GetChild(i / 2).transform.GetChild(i % 2).GetComponent<Level_Selection_script>().Lockedtext_line.SetActive(false);

                }

                //lvl_cap_server= simple lvl cap for not downloaded lvls
                if (i >= Lvl_manager.Instance.lvl_cap_server)
                {
                    if (!Check_lvl_file(i + 1))
                    {
                        content_lvl_but.transform.GetChild(i / 2).transform.GetChild(i % 2).GetComponent<Level_Selection_script>().Download_available();
                    }
                }
            }



        }
    }

    public void highlight_lvl_but(int nummm)
    {
        highligh_lvl_num = nummm;
        for (int i = 0; i < Total_level_final; i++)
        {
            if (i == nummm)
            {
                content_lvl_but.transform.GetChild(i / 2).transform.GetChild(i % 2).GetComponent<Animator>().enabled = true;
                content_lvl_but.transform.GetChild(i / 2).transform.GetChild(i % 2).GetComponent<Animator>().Play("lvl_unlocked_anim");
            }
        }
    }
    public void stop_highlight_lvl_but()
    {
        for (int i = 0; i < Total_level_final; i++)
        {
            if (i == highligh_lvl_num)
            {
                content_lvl_but.transform.GetChild(i / 2).transform.GetChild(i % 2).GetComponent<Animator>().enabled = false;
                //content_lvl_but.transform.GetChild(i / 2).transform.GetChild(i % 2).GetComponent<Animator>().sto;
                highligh_lvl_num = -1;
            }
        }
    }
    public void Setting_vlaues()
    {
        //changing coins
        //long temp_credits = long.Parse(credits.text);
        //long temp_final_value = long.Parse(PlayerPrefs.GetString("Slot_credits"));
        credits.text = long.Parse(PlayerPrefs.GetString("Slot_credits")).ToString("#,0", nfi);
        //if (PlayerPrefs.GetString("Token", string.Empty) != string.Empty)
        //{
        //    Update_API_data.Instance.is_update = 1;
        //    Update_API_data.Instance.Update_data();
        //}
        if (PlayerPrefs.GetString("scatter123", string.Empty) == string.Empty)
        {

            PlayerPrefs.SetString("scatter123", abc);
            for (int i = 0; i < array.Length; i++)
            {
                array[i] = scatter_default_value[i].ToString();
                abc += array[i];
                if (i != array.Length - 1)
                {
                    abc += ",";
                }
            }
            PlayerPrefs.SetString("scatter123", abc);
        }
        else
        {

            //print("here");
            for (int i = 0; i < array.Length; i++)
            {
                array[i] = null;
            }
            array = PlayerPrefs.GetString("scatter123").Split(',');
            for (int i = 0; i < scatter_default_value.Length; i++)
            {
                scatter_default_value[i] = long.Parse(array[i]);
            }
        }
    }

    public void Onpurchase_complete(long amount)
    {
        PlayerPrefs.SetString("Slot_credits", (long.Parse(PlayerPrefs.GetString("Slot_credits")) + amount).ToString());
        if (startup_purchase_card_panel.activeSelf)
        {
            startup_purchase_card_panel.SetActive(false);
        }
        //temp_amount = amount;
        //Setting_vlaues();
        //if (Gameplay_panel.activeSelf)
        //{
        //    Lvl_manager.Instance.OnPurchase_coins(amount);
        //}
        Shop_panel.SetActive(false);
        if (PlayerPrefs.GetString("Token", string.Empty) != string.Empty)
        {
            Update_API_data.Instance.is_update = 1;
            Update_API_data.Instance.type = "purchase";
            Update_API_data.Instance.Update_data();
        }
    }
    public void On_Rewarded_money_add(int amount)
    {
        if (home_panel.activeSelf)
        {
            PlayerPrefs.SetString("Slot_credits", (long.Parse(PlayerPrefs.GetString("Slot_credits")) + amount).ToString());
            //Setting_vlaues();
            //if (Gameplay_panel.activeSelf)
            //{
            //    Lvl_manager.Instance.OnPurchase_coins(amount);
            //}
            if (PlayerPrefs.GetString("Token", string.Empty) != string.Empty)
            {
                Update_API_data.Instance.is_update = 1;
                Update_API_data.Instance.type = "watch_video";
                Update_API_data.Instance.Update_data();
            }
        }
        //Shop_panel.SetActive(false);

    }
    public void save_scattersvalue()
    {
        //Update_Locked_theme_lvl();
        abc = string.Empty;
        for (int i = 0; i < 100; i++)
        {
            array[i] = scatter_default_value[i].ToString();
            abc += array[i];
            if (i != array.Length - 1)
            {
                abc += ",";
            }
        }
        PlayerPrefs.SetString("scatter123", abc);

    }
    public void Generate_Random_array()
    {
        scatter_default_value = new long[100];
        for (int i = 0; i < 100; i++)

        {
            int a;
            //a = UnityEngine.Random.Range(1, 6);
            //scatter_default_value[i] = (int)Random.Range(100000, Mathf.Pow(10, (6 + (a % 3))));
            if (i <= 25)
            {
                scatter_default_value[i] = (long)UnityEngine.Random.Range(100000, 10000000);
            }
            else if (i > 25 && i <= 60)
            {
                scatter_default_value[i] = (long)UnityEngine.Random.Range(10000000, 1000000000);
            }
            else if (i > 60 && i <= 90)
            {
                scatter_default_value[i] = (long)UnityEngine.Random.Range(1000000000, 5000000000);
            }
            else if (i > 90)
            {
                scatter_default_value[i] = (long)UnityEngine.Random.Range(5000000000, 10000000000);
            }


        }
    }

    public void OnLevel_select(int lvl, bool a)
    {
        if (!a)
        {
            if (lvl > Lvl_manager.Instance.lvl_cap_server)
            {
                //print(lvl);
                //print("lvl_selected" + content_lvl_but.transform.GetChild((lvl - 1) / 2).transform.GetChild((lvl - 1) % 2).transform.name);
                if (content_lvl_but.transform.GetChild((lvl - 1) / 2).transform.GetChild((lvl - 1) % 2).GetComponent<Level_Selection_script>().Download_icon.activeSelf)
                {
                    //Check_lvl_file(lvl);
                    content_lvl_but.transform.GetChild((lvl - 1) / 2).transform.GetChild((lvl - 1) % 2).GetComponent<Level_Selection_script>().Start_download();
                    // DownloadClass.Instance.Start_download(lvl);
                }
                else if (content_lvl_but.transform.GetChild((lvl - 1) / 2).transform.GetChild((lvl - 1) % 2).GetComponent<Level_Selection_script>().Download_Bg.activeSelf)
                {

                }
                else
                {
                    if (downloadable_lvl_check(lvl))
                    {
                        if (highligh_lvl_num + 1 == lvl)
                        {
                            stop_highlight_lvl_but();
                        }
                        if (Audio_manager.Instance.button.enabled)
                        {
                            Audio_manager.Instance.button.clip = Audio_manager.Instance.lvl_click;
                            Audio_manager.Instance.button.Play();
                        }
                        string asd = credits.text.Replace(" ", string.Empty);
                        PlayerPrefs.SetString("Slot_credits", asd);
                        Lvl_manager.Instance.Add_lvl_theme(lvl);
                        save_scattersvalue();
                        home_panel.SetActive(false);
                        if (Ads_COunter >= Ads_initialize_API.Instance.ads_click)
                        {
                            ads_controller.GetComponent<Ads_priority_script>().Show_interrestial();
                            Ads_COunter = 1;
                        }
                        else
                        {
                            Ads_COunter++;
                        }
                    }
                    else
                    {
                        //curropted_lvl_
                        Delete_downloadable_lvl_(lvl);
                        content_lvl_but.transform.GetChild((lvl - 1) / 2).transform.GetChild((lvl - 1) % 2).GetComponent<Level_Selection_script>().Start_download();
                    }
                }
            }
            else
            {
                if (highligh_lvl_num + 1 == lvl)
                {
                    stop_highlight_lvl_but();
                }
                if (Audio_manager.Instance.button.enabled)
                {
                    Audio_manager.Instance.button.clip = Audio_manager.Instance.lvl_click;
                    Audio_manager.Instance.button.Play();
                }
                string asd = credits.text.Replace(" ", string.Empty);
                PlayerPrefs.SetString("Slot_credits", asd);
                Lvl_manager.Instance.Add_lvl_theme(lvl);
                save_scattersvalue();
                home_panel.SetActive(false);
                if (Ads_COunter >= Ads_initialize_API.Instance.ads_click)
                {
                    ads_controller.GetComponent<Ads_priority_script>().Show_interrestial();
                    Ads_COunter = 1;
                }
                else
                {
                    Ads_COunter++;
                }
            }
        }
    }


    public bool downloadable_lvl_check(int n)
    {

        if (Lvl_manager.Instance.bundleloadrequest != null)
        {
            Lvl_manager.Instance.bundleloadrequest.Unload(true);
        }

        GameObject a;
        try
        {
            bundleloadrequest_tmp = AssetBundle.LoadFromFile(Application.persistentDataPath + "/Levels/" + n + ".lvl");
            a = bundleloadrequest_tmp.LoadAsset<GameObject>("Lvl_container");
        }
        catch
        {
            if (bundleloadrequest_tmp != null)
            {
                bundleloadrequest_tmp.Unload(true);
            }
            return false;
        }
        bundleloadrequest_tmp.Unload(true);
        return true;
    }


    public void Delete_downloadable_lvl_(int n)
    {
        string filePath = Application.persistentDataPath + "/Levels/" + n + ".lvl";

        if (System.IO.File.Exists(filePath))
        {
            // The file exists -> run event
            File.Delete(filePath);
        }
        else
        {
            // The file does not exist -> run event
        }
    }




    private void OnApplicationQuit()
    {
        //if (PlayerPrefs.GetString("Token", string.Empty) != string.Empty)
        //{
        //    Update_API_data.Instance.is_update = 1;
        //    Update_API_data.Instance.Update_data();
        //}
        save_scattersvalue();
    }

    private void OnApplicationFocus(bool focus)
    {
        //if (PlayerPrefs.GetString("Token", string.Empty) != string.Empty)
        //{
        //    Update_API_data.Instance.is_update = 1;
        //    Update_API_data.Instance.Update_data();
        //}
        save_scattersvalue();
    }
    private void OnApplicationPause(bool pause)
    {
        //if (PlayerPrefs.GetString("Token", string.Empty) != string.Empty)
        //{
        //    Update_API_data.Instance.is_update = 1;
        //    Update_API_data.Instance.Update_data();
        //}
        save_scattersvalue();
    }


    public void onlvl_up_panelon()
    {
        Bg.gameObject.transform.position = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width / 2, Screen.height, Camera.main.nearClipPlane));
        top_l.gameObject.transform.position = Camera.main.ScreenToWorldPoint(new Vector3(0, Screen.height, Camera.main.nearClipPlane));
        top_r.gameObject.transform.position = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width, Screen.height, Camera.main.nearClipPlane));
        //Vector3 a = Camera.main.ViewportToScreenPoint(lvl_up_icon.GetComponent<RectTransform>().anchoredPosition);
        //Vector3 a =  Camera.main.WorldToScreenPoint(lvl_up_icon.GetComponent<RectTransform>().anchoredPosition);
        //Vector3 a = RectTransformUtility.ScreenPointToLocalPointInRectangle(lvl_up_icon.GetComponent<RectTransform>());
        star.gameObject.transform.position = new Vector3(0, 0, Camera.main.nearClipPlane);
        Audio_manager.Instance.button.clip = Audio_manager.Instance.lvl_up;

        Audio_manager.Instance.button.Play();
        StartCoroutine("Change_button_sound");

        Bg.gameObject.SetActive(true);
        Bg.Play();
        top_l.gameObject.SetActive(true);
        top_l.Play();
        top_r.gameObject.SetActive(true);
        top_r.Play();
        StartCoroutine(waitfor_animation());
    }

    IEnumerator Change_button_sound()
    {
        //AudioSource audio = GetComponent<AudioSource>();

        //audio.Play();
        yield return new WaitForSeconds(Audio_manager.Instance.slot_sound.clip.length);
        Audio_manager.Instance.button.clip = Audio_manager.Instance.lvl_click;
        //Audio_manager.Instance.slot_sound.clip = Audio_manager.Instance.spin;
        //audio.clip = ;
        //audio.Play();
    }
    public void off_lvl_up_panel()
    {
        Bg.gameObject.SetActive(false);
        top_l.gameObject.SetActive(false);
        top_r.gameObject.SetActive(false);
        star.gameObject.SetActive(false);
    }


    IEnumerator waitfor_animation()
    {
        yield return new WaitForSecondsRealtime(1f);
        star.gameObject.SetActive(true);
        star.Play();
    }



    public void Generate_shop_panel()
    {
        if (Content_Shop_panel.transform.childCount > 0)
        {
            foreach (Transform a in Content_Shop_panel.transform)
            {
                Destroy(a);
            }
        }
        //Debug.Log("Get_API_Data_IAP.Instance.product_id.Length" + Get_API_Data_IAP.Instance.product_id.Length);
        for (int i = 0; i < Get_API_Data_IAP.Instance.product_id.Length; i++)
        {
            //Debug.Log(i);
            GameObject ab = Instantiate(purchaser_container, Vector3.zero, Quaternion.identity);
            ab.transform.SetParent(Content_Shop_panel.transform);
            ab.transform.localScale = new Vector3(1, 1, 1);
            ab.GetComponent<Purchase_container_script>().Main_value.text = Get_API_Data_IAP.Instance.first_val[i].ToString();
            ab.GetComponent<Purchase_container_script>().Percentage_value.text = Get_API_Data_IAP.Instance.percentage_val[i].ToString() + "%";
            ab.GetComponent<Purchase_container_script>().Buy_value.text = GetComponent<InAppManager>().GetProductPriceFromStore( Get_API_Data_IAP.Instance.product_id[i]);
            ab.GetComponent<Purchase_container_script>().Final_value.text = (Get_API_Data_IAP.Instance.first_val[i] + ((Get_API_Data_IAP.Instance.first_val[i] * Get_API_Data_IAP.Instance.percentage_val[i]) / 100)).ToString();
            ab.GetComponent<Purchase_container_script>().number = i;
            //ab.GetComponent<Purchase_container_script>().buy_button.GetComponent<Button>().onClick.AddListener(() => GetComponent<InAppManager>().BuyProductID(Get_API_Data_IAP.Instance.product_id[i]));
            ab.GetComponent<Purchase_container_script>().ONCLICK_but();
        }
    }

    public void on_startup_purchase_card()
    {
        //int ax = Get_API_Data_IAP.Instance.first_val.Length - 1;
        startup_purchase_card_panel.GetComponent<Purchase_container_script>().Main_value.text = Get_API_Data_IAP.Instance.St_purchase_first_val.ToString();
        startup_purchase_card_panel.GetComponent<Purchase_container_script>().Percentage_value.text = Get_API_Data_IAP.Instance.St_purchase_percentage_val.ToString() + "%\nOff";
        startup_purchase_card_panel.GetComponent<Purchase_container_script>().Buy_value.text = GetComponent<InAppManager>().GetProductPriceFromStore(Get_API_Data_IAP.Instance.St_purchase_product_id);
        startup_purchase_card_panel.GetComponent<Purchase_container_script>().Final_value.text = (Get_API_Data_IAP.Instance.St_purchase_first_val + ((Get_API_Data_IAP.Instance.St_purchase_first_val * Get_API_Data_IAP.Instance.St_purchase_percentage_val) / 100)).ToString();
        startup_purchase_card_panel.GetComponent<Purchase_container_script>().number = -1;
        startup_purchase_card_panel.GetComponent<Purchase_container_script>().ONCLICK_but();
        //Turn_on_startup_panel();
        Get_API_Data_IAP.Instance.startup_purchase_card();
    }

    public void Turn_on_startup_panel()
    {
        //if(startup_purchase_card_panel.GetComponent<Purchase_container_script>().Buy_value.text!= string.Empty)
        //{
        if (home_panel.activeSelf)
        {
            startup_purchase_card_panel.SetActive(true);
        }
        else
        {
            Splash_Panel_scripts.Instance.startup_panel_ready = true;
        }
        //}
    }
    public bool Check_lvl_file(int x)
    {
        if (!Directory.Exists(Application.persistentDataPath + "/Levels"))
        {
            //if it doesn't, create it
            Directory.CreateDirectory(Application.persistentDataPath + "/Levels");
            //Instantiate(ac, Vector3.zero, Quaternion.identity);

        }
        string filePath = Application.persistentDataPath + "/Levels/" + x + ".lvl";

        if (System.IO.File.Exists(filePath))
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    //public void Set_download_fill_value(int l, int progress)
    //{
    //    float x = (float)progress / 100f;
    //    content_lvl_but.transform.GetChild((l - 1) / 2).transform.GetChild((l - 1) % 2).GetComponent<Level_Selection_script>().Set_fill_amount(x);
    //}

    //public void DOwnloadComplete(int c)
    //{
    //    content_lvl_but.transform.GetChild((c - 1) / 2).transform.GetChild((c - 1) % 2).GetComponent<Level_Selection_script>().Download_Complete();
    //}


    public void Privacy_policy_open()
    {
        Application.OpenURL(Get_API_Data_IAP.Instance.privacy_policy);
    }

    // Update is called once per frame
    void Update()
    {
        // level.text = Lvl_manager.Instance.level.text;
        //Exception as;
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (Gameplay_panel.activeSelf)
            {
                if (Lobby_exit_panel.activeSelf)
                {
                    Lobby_exit_panel.SetActive(false);
                }
                else
                {
                    Lobby_exit_panel.SetActive(true);
                }//Lvl_manager.Instance.Lobby_click();
            }
            else if (home_panel.activeSelf)
            {
                //exitPanel
                Exit_game_panel.SetActive(true);
            }
            else if (splash_panel.activeSelf)
            {
                Application.Quit();
            }

        }
    }

    public void Exit_game()
    {
        Application.Quit();
    }
}

