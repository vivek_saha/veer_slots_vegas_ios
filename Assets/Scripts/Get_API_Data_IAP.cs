﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SimpleJSON;
using UnityEngine.Networking;
using System.IO;
using System;
using IAP;
using TMPro;
using UnityEngine.UI;

public class Get_API_Data_IAP : MonoBehaviour
{

    public static Get_API_Data_IAP Instance;

    // API url
    string url = "http://134.209.103.120/Casino/mapi/1/app-settings/eyJ0eXAiOiJKV1QiLCdlcMdGciOiJSUzI1";

    // resulting JSON from an API request
    public JSONNode jsonResult;

    // create the web request and download handler
    //UnityWebRequest webReq = new UnityWebRequest();

    string rawJson;


    //public 

    [HideInInspector]
    public long[] first_val;
    [HideInInspector]
    public int[] percentage_val;
    [HideInInspector]
    public string[] price_val;
    [HideInInspector]
    public string[] product_id;



    public long St_purchase_first_val;
    public int St_purchase_percentage_val;
    public string St_purchase_price_val;
    public string St_purchase_product_id;
    //public bool success;
    public int Total_lvl_server;
    public long fb_coin;
    public long guest_coin;
    public long credits;
    public long currentxp;
    public int app_version;
    public int is_undermaintenance;
    public string app_update_message;
    public string android_app_link;
    public string ios_app_link;
    public string privacy_policy;
    public string app_message;
    public string fb_share_link_title;
    public string share_private_link_message;
    public string terms_and_conditions;
    public int watch_video_coin;
    public bool is_daily_bonus;

    ////ads

    //public string android_admob_interstitial_id;
    //public string ios_admob_interstitial_id;
    //public string android_admob_reward_id;
    //public string ios_admob_reward_id;


    //public string android_facebook_reward_id;
    //public string android_facebook_interstitial_id;



    //public string android_unity_game_id;
    ////public string ios_unity_game_id;


    //public string ads_priority;
    //public string interstitial_ads_priority;
    //public int ads_click;
    public bool req_call = true;

    public string updated_level;

    //userdata
    public string Username;
    public string profile_pic_link;
    public long wallet_coin;
    public int current_xp;
    public int is_unlocked = 0;
    public int is_blocked = 0;
    public int start;
    public int end;
    //public string token;
    private void Awake()
    {
        Instance = this;

    }


    private void Start()
    {
    }
    public void Get_settings_data()
    {
        Shop_API_script.Instance.Get_settings_data();
        //Debug.Log("getdata");
        StartCoroutine("GetData");

    }



    // sends an API request - returns a JSON file
    void Manual_Start()
    {

        jsonResult = JSON.Parse(rawJson);
        Set_Json_data();
        Invoke("temp_waiter",0.5f);
        

    }
    public void temp_waiter()
    {
        if (is_undermaintenance == 0)
        {
            if (Version_check())
            {
                //temp_before_login_start
                if (is_blocked == 0)
                {
                    Normal_start();
                }
            }
            else
            {
                UPdate_app_notice();
            }
        }
        else
        {
            //app_message = jsonResult["data"]["app_config"]["app_message"].Value;
            Undermaintenance_Notice();
        }
    }


    public void Set_Json_data()
    {
        is_undermaintenance = jsonResult["data"]["app_config"]["ios_is_undermaintenance"].AsInt;
        app_message = jsonResult["data"]["app_config"]["app_message"].Value;
        android_app_link = jsonResult["data"]["app_config"]["android_app_link"].Value;
        ios_app_link = jsonResult["data"]["app_config"]["ios_app_link"].Value;
        privacy_policy = jsonResult["data"]["app_config"]["privacy_policy"].Value;
        terms_and_conditions = jsonResult["data"]["app_config"]["privacy_policy"].Value;
        fb_share_link_title = jsonResult["data"]["app_config"]["fb_share_link_title"].Value;
        share_private_link_message = jsonResult["data"]["app_config"]["share_private_link_message"].Value;
        guest_coin = jsonResult["data"]["app_config"]["guest_coin"].AsLong;
        watch_video_coin = jsonResult["data"]["app_config"]["watch_video_coin"].AsInt;
        fb_coin = jsonResult["data"]["app_config"]["fb_coin"].AsLong;
        Total_lvl_server = jsonResult["data"]["app_config"]["total_level"].AsInt;
        updated_level = jsonResult["data"]["app_config"]["updated_level"].Value;
        start = jsonResult["data"]["app_config"]["hourly_reward_coin"]["start"].AsInt;
        end = jsonResult["data"]["app_config"]["hourly_reward_coin"]["end"].AsInt;
        Check_updated_lvl();
        //int m = 0;
        //foreach (JSONNode message in jsonResult["data"]["in_app_purchase_root"]["ios_in_app_purchase"])
        //{
        //    m++;
        //}

        ////Debug.Log("m" + m);
        //first_val = new long[m];
        //percentage_val = new int[m];
        //price_val = new string[m];
        //product_id = new string[m];


        //int counter = 0;
        //foreach (JSONNode message in jsonResult["data"]["in_app_purchase_root"]["ios_in_app_purchase"])
        //{

        //    first_val[counter] = message["val"].AsLong;
        //    percentage_val[counter] = message["percentage"].AsInt;
        //    price_val[counter] = message["price"].Value;
        //    product_id[counter] = message["product_id"].Value;

        //    counter++;
        //}


        //St_purchase_first_val = jsonResult["data"]["ios_offer"]["val"].AsLong;
        //St_purchase_percentage_val = jsonResult["data"]["ios_offer"]["percentage"].AsInt;
        //St_purchase_price_val = jsonResult["data"]["ios_offer"]["price"].Value;
        //St_purchase_product_id = jsonResult["data"]["ios_offer"]["product_id"].Value;

        ////ADS

        ////admob
        //android_admob_interstitial_id = jsonResult["data"]["ads"]["android_admob_interstitial_id"].Value;
        //ios_admob_interstitial_id = jsonResult["data"]["ads"]["ios_admob_interstitial_id"].Value;
        //android_admob_reward_id= jsonResult["data"]["ads"]["android_admob_reward_id"].Value;
        ////ios_admob_reward_id= jsonResult["data"]["ads"]["ios_admob_reward_id"].Value;

        ////fb
        //android_facebook_interstitial_id= jsonResult["data"]["ads"]["android_facebook_interstitial_id"].Value;
        //android_facebook_reward_id= jsonResult["data"]["ads"]["android_facebook_reward_id"].Value;



        ////unity
        //android_unity_game_id = jsonResult["data"]["ads"]["android_unity_game_id"].Value;
        ////ios_unity_game_id = jsonResult["data"]["ads"]["ios_unity_game_id"].Value;



        //ads_click = jsonResult["data"]["ads"]["ads_click"].AsInt;
        //ads_priority= jsonResult["data"]["ads"]["ads_priority"].Value;
        //interstitial_ads_priority= jsonResult["data"]["ads"]["interstitial_ads_priority"].Value;
        //SEt ALL IDS TO scripts




        //userDATA
        is_blocked = jsonResult["data"]["user"]["is_blocked"].AsInt;


        Username = jsonResult["data"]["user"]["name"].Value;
        PlayerPrefs.SetString("Username", Username);
        profile_pic_link = jsonResult["data"]["user"]["profile_picture"].Value;
        PlayerPrefs.SetString("profile_pic_link_2", profile_pic_link);
        wallet_coin = jsonResult["data"]["user"]["wallet_coin"].AsLong;
        Gamemanager.Instance.coin_change_checker = wallet_coin;
        PlayerPrefs.SetString("Slot_credits", wallet_coin.ToString());
        current_xp = jsonResult["data"]["user"]["current_xp"].AsInt;
        Gamemanager.Instance.xp_change_checker = current_xp;
        PlayerPrefs.SetInt("CurrentXP", current_xp);
        is_unlocked = jsonResult["data"]["user"]["is_unlocked"].AsInt;
        is_daily_bonus= jsonResult["data"]["user"]["is_daily_bonus"].AsBool;
        if(is_daily_bonus)
        {
            //disable_daily_bonus
            Gamemanager.Instance.transform.GetComponent<DailyReward>().disable_dialy_bonus_fromgetapi_data();
        }
        else
        {
            Gamemanager.Instance.transform.GetComponent<DailyReward>().enable_dialy_bonus_fromgetapi_data();
        }
        //Debug.Log("is_unlocked"+is_unlocked);

        if (is_blocked == 0)
        {
            Set_User_data.Instance.setdata_after_get();
        }
        else if (is_blocked == 1)
        {
            Splash_Panel_scripts.Instance.BLoked_fun();
        }
    }
    public void Normal_start()
    {
        //Debug.Log("called");
        //#if 

        //#endif
        Gamemanager.Instance.Total_level_final = Total_lvl_server;
        if (is_unlocked == 1)
        {
            Gamemanager.Instance.Unlocked_all_lvl_test = true;
        }
        Gamemanager.Instance.manual_Start();
        //Gamemanager.Instance.Generate_shop_panel();
        //Gamemanager.Instance.gameObject.transform.GetComponent<InAppManager>().manual_start();
        //startup_purchase_card();
        //Gamemanager.Instance.ads_controller.GetComponent<AdsController>().Manual_Start();
        //manual_start all ads
        //Manual_start_ALL_ADS();
    }

    public void startup_purchase_card()
    {
        if(current_xp>10000)
        {
            Gamemanager.Instance.Turn_on_startup_panel();
        }
    }

    //public void Manual_start_ALL_ADS()
    //{
    //    Gamemanager.Instance.ads_controller.GetComponent<AdsController>().Manual_Start();
    //    Gamemanager.Instance.ads_controller.GetComponent<Unity_Ads_script>().Manual_Start();
    //    Gamemanager.Instance.ads_controller.GetComponent<FB_ADS_script>().Manual_Start();

    //}

    public void SEt_all_ads_ids()
    {
        //Gamemanager.Instance.ads_controller.GetComponent<>
    }


    public bool Version_check()
    {
        app_version = jsonResult["data"]["app_config"]["ios_app_version"].AsInt;
        //Debug.Log(Application.version);
        if (Gamemanager.Instance.App_ver_code >= app_version)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    //public static bool IsConnected(string hostedURL = "http://www.google.com")
    //{
    //    try
    //    {
    //        string HtmlText = GetHtmlFromUri(hostedURL);
    //        if (HtmlText == "")
    //            return false;
    //        else
    //            return true;
    //    }
    //    catch (IOException ex)
    //    {
    //        return false;
    //    }
    //}

    IEnumerator GetData()
    {
        UnityWebRequest www = UnityWebRequest.Get(url);
        req_call = false;
        www.SetRequestHeader("Authorization", PlayerPrefs.GetString("Token"));
        //Debug.Log("Token : "+ PlayerPrefs.GetString("Token"));
        yield return www.Send();
        //Debug.Log(www);
        if (www.isNetworkError)
        {
            Debug.Log(www.error);


            Check_Internet_connection();
            req_call = true;
        }
        else
        {
            close_Retry_panel();
            // Show results as text
            //Debug.Log(www.downloadHandler.text);
            rawJson = www.downloadHandler.text;
            Manual_Start();
            // Or retrieve results as binary data
            // byte[] results = www.downloadHandler.data;
        }
    }


    // Update is called once per frame
    void Update()
    {
        //if (req_call)
        //{
        //    StartCoroutine("GetData");
        //}
    }

    public void Undermaintenance_Notice()
    {
        Splash_Panel_scripts.Instance.Notice_panel.SetActive(true);
        Splash_Panel_scripts.Instance.UnderMaintenance_panel.GetComponentInChildren<TextMeshProUGUI>().text = app_message;
        Splash_Panel_scripts.Instance.UnderMaintenance_panel.GetComponentInChildren<Button>().onClick.AddListener(() => Close_app());
        Splash_Panel_scripts.Instance.UnderMaintenance_panel.SetActive(true);

    }



    public void UPdate_app_notice()
    {
        Splash_Panel_scripts.Instance.Notice_panel.SetActive(true);
        Splash_Panel_scripts.Instance.Update_notice_Panel.GetComponentInChildren<TextMeshProUGUI>().text = app_message;
        android_app_link = jsonResult["data"]["app_config"]["android_app_link"].Value;
        Splash_Panel_scripts.Instance.Update_notice_Panel.GetComponentInChildren<Button>().onClick.AddListener(() => Update_app());
        Splash_Panel_scripts.Instance.Update_notice_Panel.SetActive(true);

    }
    public void Close_app()
    {
        Application.Quit();
    }

    public void Update_app()
    {

        print("called");
        Application.OpenURL(android_app_link);
    }
    public void Check_Internet_connection()
    {
        StartCoroutine(Ck_net(isConnected =>
        {
            if (isConnected)
            {
                Debug.Log("Internet Available!");
                //return true;
                if (!Splash_Panel_scripts.Instance.Loading_panel.activeSelf)
                {
                    Splash_Panel_scripts.Instance.Notice_panel.SetActive(true);
                    Splash_Panel_scripts.Instance.Something_went_wrong_panel.GetComponentInChildren<Button>().onClick.AddListener(() => Retry_connection());
                    Splash_Panel_scripts.Instance.Something_went_wrong_panel.SetActive(true);
                }
            }
            else
            {
                Debug.Log("Internet Not Available");
                //return  false;
                if (!Splash_Panel_scripts.Instance.Loading_panel.activeSelf)
                {
                    Splash_Panel_scripts.Instance.Notice_panel.SetActive(true);
                    Splash_Panel_scripts.Instance.no_internet_connection_panel.GetComponentInChildren<Button>().onClick.AddListener(() => Retry_connection());
                    Splash_Panel_scripts.Instance.no_internet_connection_panel.SetActive(true);
                }
            }
        }));
    }

    public IEnumerator Ck_net(Action<bool> syncResult)
    {
        const string echoServer = "http://google.com";

        bool result;
        using (var request = UnityWebRequest.Head(echoServer))
        {
            request.timeout = 0;
            yield return request.SendWebRequest();
            result = !request.isNetworkError && !request.isHttpError && request.responseCode == 200;
        }
        syncResult(result);
    }
    //public bool check_net()
    //{
    //    WWW www = new WWW("http://www.google.com");
    //    //yield return www;
    //    if (www.error != null)
    //    {
    //        return false;
    //    }
    //    else
    //    {
    //        return true;
    //    }

    //}
    public void Retry_connection()
    {
        Get_settings_data();
        Splash_Panel_scripts.Instance.Something_went_wrong_panel.SetActive(false);
        Splash_Panel_scripts.Instance.no_internet_connection_panel.SetActive(false);
        Splash_Panel_scripts.Instance.Loading_panel.SetActive(true);
        StartCoroutine("Retry_connection_coroutine");
    }
    IEnumerator Retry_connection_coroutine()
    {
        yield return new WaitForSeconds(3f);
        //StartCoroutine("check_connection");
        Splash_Panel_scripts.Instance.Loading_panel.SetActive(false);
        Splash_Panel_scripts.Instance.Notice_panel.SetActive(false);
        Get_settings_data();
    }
    public void close_Retry_panel()
    {
        Splash_Panel_scripts.Instance.Something_went_wrong_panel.SetActive(false);
        Splash_Panel_scripts.Instance.no_internet_connection_panel.SetActive(false);
        Splash_Panel_scripts.Instance.Loading_panel.SetActive(false);
        Splash_Panel_scripts.Instance.Notice_panel.SetActive(false);
    }


    public void Check_updated_lvl()
    {
        if (PlayerPrefs.GetString("updated_level", "0") == updated_level)
        {

        }
        else
        {
            string full_str = updated_level;
            string[] az = full_str.Split(',');
            foreach (string a in az)
            {
                FileChk_lvl(a);
            }
            PlayerPrefs.SetString("updated_level", updated_level);
        }
    }

    public void FileChk_lvl(string lvl_name)
    {
        if (!Directory.Exists(Application.persistentDataPath + "/Levels"))
        {
            //if it doesn't, create it
            Directory.CreateDirectory(Application.persistentDataPath + "/Levels");

        }

        else
        {
            string filePath = Application.persistentDataPath + "/Levels/" + lvl_name + ".lvl";

            if (System.IO.File.Exists(filePath))
            {
                // The file exists -> run event
                File.Delete(filePath);
            }
            else
            {
                // The file does not exist -> run event
            }
        }
    }

}




