﻿using SimpleJSON;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class LeaderBoard_API_Call : MonoBehaviour
{
    public Start_lederboard_panel sc;
    public GameObject Leaderboard_prefab;
    public Transform leaderboard_content;

    public JSONNode jsonResult;


    string u_name, code;
    // create the web request and download handler
    //UnityWebRequest webReq = new UnityWebRequest();

    string url = "http://134.209.103.120/Casino/mapi/1/get-leaderboard";
    string rawJson;
    // Start is called before the first frame update
    void Start()
    {

    }

    public void Onleaderboard_click()
    {
        sc.Manual_Start();
        if(leaderboard_content.childCount>0)
        {
           for(int i =0;i<leaderboard_content.childCount;i++)
            {
                Destroy(leaderboard_content.GetChild(i).gameObject);
            }
           
        }
        StartCoroutine("GetData");
    }

    IEnumerator GetData()
    {
        UnityWebRequest www = UnityWebRequest.Get(url);
        //req_call = false;
        www.SetRequestHeader("Authorization", PlayerPrefs.GetString("Token"));
        yield return www.Send();
        Debug.Log(www.responseCode);
        if (www.isNetworkError)
        {
            Debug.Log(www.error);


            Check_Internet_connection();
            //req_call = true;
        }
        else
        {
            close_Retry_panel();
            // Show results as text
            //Debug.Log(www.downloadHandler.text);
            rawJson = www.downloadHandler.text;
            //Manual_Start();
            Set_Json_data();
            // Or retrieve results as binary data
            // byte[] results = www.downloadHandler.data;
        }
    }

    public void Set_Json_data()
    {
        GameObject temp;
        jsonResult = JSON.Parse(rawJson);

        int counter = 1;
        foreach (JSONNode data in jsonResult["data"]["toppers"])
        {
            temp = Instantiate(Leaderboard_prefab, Vector3.zero, Quaternion.identity);
            temp.transform.parent = leaderboard_content.transform;
            temp.transform.localScale = new Vector3(1, 1, 1);
            temp.GetComponent<LeaderBoard_container_script>().Number.text = counter.ToString();
            temp.GetComponent<LeaderBoard_container_script>().profile_pic_link_2 = data["profile_picture"].Value;
            Set_username(data["name"].Value);
            temp.GetComponent<LeaderBoard_container_script>().Name.text = u_name;
            temp.GetComponent<LeaderBoard_container_script>().code.text = code;
            temp.GetComponent<LeaderBoard_container_script>().lvl_text.text = (computeLevel(data["current_xp"].AsInt)).ToString();
            temp.GetComponent<LeaderBoard_container_script>().coin.text = AbbrevationUtility_LeaderBoard.AbbreviateNumber(data["wallet_coin"].AsFloat);
            temp = null;
            counter++;
        }



    }
    // Update is called once per frame
    void Update()
    {

    }
    public void Set_username(string Username_server)
    {
        string rev = Reverse(Username_server);
        string[] splitArray = rev.Split(char.Parse("_"));
        code = Reverse(splitArray[0]);
        code = "_" + code;
        u_name = string.Empty;
        for (int i = 1; i < splitArray.Length; i++)
        {
            if (i == 1)
            {
                u_name += splitArray[i];
            }
            else
            {
                u_name += "_" + splitArray[i];
            }
        }
        u_name = Reverse(u_name);


        //username.text = u_name + "<size= 25><#828282>" + code + "</color></size>";
        //PlayerPrefs.SetString("", Send_Data_to_SErver.Instance.email);
    }
    public static string Reverse(string s)
    {
        char[] charArray = s.ToCharArray();
        Array.Reverse(charArray);
        return new string(charArray);
    }
    int computeLevel(int currentXP)
    {
        float lvl = Mathf.Sqrt(currentXP / Lvl_manager.Instance.leveling.xpBase);



        //print("lvl" + lvl);
        int clevel = Mathf.FloorToInt(lvl + 1);
        //lvl_up_check(clevel);
        if (clevel == 0) clevel = 1;
        return clevel;
    }
    public void Check_Internet_connection()
    {
        StartCoroutine(Ck_net(isConnected =>
        {
            if (isConnected)
            {
                Debug.Log("Internet Available!");
                //return true;
                if (!Splash_Panel_scripts.Instance.Loading_panel.activeSelf)
                {
                    Splash_Panel_scripts.Instance.Notice_panel.SetActive(true);
                    Splash_Panel_scripts.Instance.Something_went_wrong_panel.GetComponentInChildren<Button>().onClick.AddListener(() => Retry_connection());
                    Splash_Panel_scripts.Instance.Something_went_wrong_panel.SetActive(true);
                }
            }
            else
            {
                Debug.Log("Internet Not Available");
                //return  false;
                if (!Splash_Panel_scripts.Instance.Loading_panel.activeSelf)
                {
                    Splash_Panel_scripts.Instance.Notice_panel.SetActive(true);
                    Splash_Panel_scripts.Instance.no_internet_connection_panel.GetComponentInChildren<Button>().onClick.AddListener(() => Retry_connection());
                    Splash_Panel_scripts.Instance.no_internet_connection_panel.SetActive(true);
                }
            }
        }));
    }

    public IEnumerator Ck_net(Action<bool> syncResult)
    {
        const string echoServer = "http://google.com";

        bool result;
        using (var request = UnityWebRequest.Head(echoServer))
        {
            request.timeout = 0;
            yield return request.SendWebRequest();
            result = !request.isNetworkError && !request.isHttpError && request.responseCode == 200;
        }
        syncResult(result);
    }
    //public bool check_net()
    //{
    //    WWW www = new WWW("http://www.google.com");
    //    //yield return www;
    //    if (www.error != null)
    //    {
    //        return false;
    //    }
    //    else
    //    {
    //        return true;
    //    }

    //}
    public void Retry_connection()
    {
        StartCoroutine("GetData");
        Splash_Panel_scripts.Instance.Something_went_wrong_panel.SetActive(false);
        Splash_Panel_scripts.Instance.no_internet_connection_panel.SetActive(false);
        Splash_Panel_scripts.Instance.Loading_panel.SetActive(true);
        StartCoroutine("Retry_connection_coroutine");
    }
    IEnumerator Retry_connection_coroutine()
    {
        yield return new WaitForSeconds(3f);
        //StartCoroutine("check_connection");
        Splash_Panel_scripts.Instance.Loading_panel.SetActive(false);
        Splash_Panel_scripts.Instance.Notice_panel.SetActive(false);
        StartCoroutine("GetData");
    }
    public void close_Retry_panel()
    {
        Splash_Panel_scripts.Instance.Something_went_wrong_panel.SetActive(false);
        Splash_Panel_scripts.Instance.no_internet_connection_panel.SetActive(false);
        Splash_Panel_scripts.Instance.Loading_panel.SetActive(false);
        Splash_Panel_scripts.Instance.Notice_panel.SetActive(false);
    }
}
