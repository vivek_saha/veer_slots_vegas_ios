﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class profile_panel_temp_value_script : MonoBehaviour
{

    public TextMeshProUGUI lvl_text;
    public TextMeshProUGUI coin_value;
    // Start is called before the first frame update
    void Start()
    {
        lvl_text.text = Gamemanager.Instance.level.text;
        coin_value.text = Gamemanager.Instance.credits.text;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
