﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System.Linq;
using System.Globalization;

public class Level_Selection_script : MonoBehaviour
{
    public enum typeofscatter { mini, big, grand };
    typeofscatter ac = typeofscatter.mini;
    public int lvl_num = 1;
    public bool locked = false;
    public TextMeshProUGUI scatter_text,lvl_text;
    long scatters;
    public GameObject Icon_frame, Lockedtext_line, locked_image,main_image,Download_icon,Download_Bg;
    public Image Filling_image;
    public Sprite[] Frames; 

    NumberFormatInfo nfi;

    // Start is called before the first frame update
    void Start()
    {
        nfi = (NumberFormatInfo)CultureInfo.InvariantCulture.NumberFormat.Clone();
        nfi.NumberGroupSeparator = " ";

        GetComponent<Button>().onClick.AddListener(() => Gamemanager.Instance.OnLevel_select(lvl_num, locked));
        //print(lvl_num-1);
        lvl_text.text = lvl_num.ToString();
        scatters = Gamemanager.Instance.scatter_default_value[lvl_num - 1];
        long x = Gamemanager.Instance.scatter_default_value[lvl_num - 1] / 10000;
        if (x < 100)
        {
            ac = typeofscatter.mini;
        }
        else if (x > 100 && x < 10000)
        {
            ac = typeofscatter.big;
        }
        else if (x > 10000)
        {
            ac = typeofscatter.grand;
        }

    }
    void Update()
    {
        if(scatters< Gamemanager.Instance.scatter_default_value[lvl_num - 1])
        {
            scatters = Gamemanager.Instance.scatter_default_value[lvl_num - 1];
        }
        switch (ac)
        {
            case typeofscatter.mini:
                scatters += 1;
                break;
            case typeofscatter.big:
                scatters += 11;
                break;
            case typeofscatter.grand:
                scatters += 111;
                break;
            default:
                break;
        }
        show_scattertext();
    }

    public void show_scattertext()
    {
        Gamemanager.Instance.scatter_default_value[lvl_num - 1] = scatters;
        scatter_text.text = scatters.ToString("#,0", nfi);
    }

    public void Download_available()
    {
        if(!locked)
        {
            Download_Bg.SetActive(false);
            Download_icon.SetActive(true);
        }
    }


    public void Start_download()
    {
        Download_icon.SetActive(false);
        Download_Bg.SetActive(true);
        Filling_image.fillAmount = 0;
        GetComponent<DownloadClass>().Start_download(lvl_num);
    }


    public void Set_fill_amount(float a)
    {
        Filling_image.fillAmount = a;
    }
    public void Download_Complete()
    {
        Download_Bg.SetActive(false);
    }
}