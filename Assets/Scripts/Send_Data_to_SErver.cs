﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using System;
using SimpleJSON;
using UnityEngine.UI;

public class Send_Data_to_SErver : MonoBehaviour
{
    public static Send_Data_to_SErver Instance;

    public string fb_id;
    public string login_type;
    public string device_token;
    public string email;
    public string Name;
    //public string country_code;
    public string profile_picture_link;
    public long wallet_coin;
    public int current_xp;


    public string textname;
    public Texture2D mytexture;
    public string Token_autorization;
    byte[] bytes;

    public JSONNode jsonResult;

    string rawJson;
    private void Awake()
    {
        Instance = this;
    }


    private void Start()
    {
        device_token = SystemInfo.deviceUniqueIdentifier;

    }
    public void Upload_data()
    {
        StartCoroutine(Upload());
    }

    IEnumerator Upload()
    {
        //Debug.Log("device_token" + device_token);
        WWWForm form = new WWWForm();
        form.AddField("login_type", login_type);
        form.AddField("name", Name);
        form.AddField("device_token", device_token);
        if (login_type == "google")
        {
            form.AddField("fb_id", fb_id);
            form.AddField("email", email);
            form.AddField("profile_picture", profile_picture_link);
        }
        if(login_type == "apple")
        {
            form.AddField("fb_id", fb_id);
            form.AddField("email", email);
            form.AddBinaryData("profile_picture", Guest_login_script.Instance.itemBGBytes, "temp.png", "image/png");
        }

        form.AddField("wallet_coin", wallet_coin.ToString());
        form.AddField("current_xp", current_xp);

        if (login_type == "guest")
        {
            form.AddBinaryData("profile_picture", Guest_login_script.Instance.itemBGBytes, "temp.png", "image/png");
        }
        UnityWebRequest www = UnityWebRequest.Post("http://134.209.103.120/Casino/mapi/1/login", form);
        yield return www.SendWebRequest();
        Debug.Log("www: " + www.responseCode);
        if (www.isNetworkError || www.isHttpError)
        {
            Debug.Log(www.error);
            Check_Internet_connection();
        }
        else
        {
            close_Retry_panel();
            rawJson = www.downloadHandler.text;
            Get_response_data();
            Debug.Log("Form upload complete!");
        }

    }

    public void Get_response_data()
    {
        jsonResult = JSON.Parse(rawJson);
        //is_undermaintenance = jsonResult["data"]["app_config"]["is_undermaintenance"].AsInt;
        fb_id = jsonResult["data"]["id"].Value;
        Set_User_data.Instance.Username_server = Name = jsonResult["data"]["name"].Value;
        email = jsonResult["data"]["email"].Value;
        Set_User_data.Instance.credits = wallet_coin = jsonResult["data"]["wallet_coin"].AsLong;
        Set_User_data.Instance.currentxp = current_xp = jsonResult["data"]["current_xp"].AsInt;
        login_type = jsonResult["data"]["login_type"].Value;
        Set_User_data.Instance.profile_pic_link_2 = jsonResult["data"]["profile_picture"].Value;


        Token_autorization = "Bearer " + jsonResult["data"]["token"].Value;
        PlayerPrefs.SetString("Token", Token_autorization);
        //Debug.Log("Token_autorization : " + Token_autorization);
        Set_User_data.Instance.Set_data();
        //Set_User_data.Instance.


    }

    public void Check_Internet_connection()
    {
        StartCoroutine(Ck_net(isConnected =>
        {
            if (isConnected)
            {
                Debug.Log("Internet Available!");
                //return true;
                if (!Splash_Panel_scripts.Instance.Loading_panel.activeSelf)
                {
                    Splash_Panel_scripts.Instance.Notice_panel.SetActive(true);
                    Splash_Panel_scripts.Instance.Something_went_wrong_panel.GetComponentInChildren<Button>().onClick.AddListener(() => Retry_connection());
                    Splash_Panel_scripts.Instance.Something_went_wrong_panel.SetActive(true);
                }
            }
            else
            {
                Debug.Log("Internet Not Available");
                //return  false;
                if (!Splash_Panel_scripts.Instance.Loading_panel.activeSelf)
                {
                    Splash_Panel_scripts.Instance.Notice_panel.SetActive(true);
                    Splash_Panel_scripts.Instance.no_internet_connection_panel.GetComponentInChildren<Button>().onClick.AddListener(() => Retry_connection());
                    Splash_Panel_scripts.Instance.no_internet_connection_panel.SetActive(true);
                }
            }
        }));
    }


   

    public IEnumerator Ck_net(Action<bool> syncResult)
    {
        const string echoServer = "http://google.com";

        bool result;
        using (var request = UnityWebRequest.Head(echoServer))
        {
            request.timeout = 0;
            yield return request.SendWebRequest();
            result = !request.isNetworkError && !request.isHttpError && request.responseCode == 200;
        }
        syncResult(result);
    }
    public void Retry_connection()
    {
        Upload_data();
        Splash_Panel_scripts.Instance.Something_went_wrong_panel.SetActive(false);
        Splash_Panel_scripts.Instance.no_internet_connection_panel.SetActive(false);
        Splash_Panel_scripts.Instance.Loading_panel.SetActive(true);
        StartCoroutine("Retry_connection_coroutine");
    }
    IEnumerator Retry_connection_coroutine()
    {
        yield return new WaitForSeconds(3f);
        //StartCoroutine("check_connection");
        Splash_Panel_scripts.Instance.Loading_panel.SetActive(false);
        Splash_Panel_scripts.Instance.Notice_panel.SetActive(false);
        Upload_data();
    }
    public void close_Retry_panel()
    {
        Splash_Panel_scripts.Instance.Something_went_wrong_panel.SetActive(false);
        Splash_Panel_scripts.Instance.no_internet_connection_panel.SetActive(false);
        Splash_Panel_scripts.Instance.Loading_panel.SetActive(false);
        Splash_Panel_scripts.Instance.Notice_panel.SetActive(false);
    }
    //public void get_data_from_server()
    //{
    //    StartCoroutine("Get_data");
    //}

    //IEnumerator Get_data()
    //{
    //    WWWForm form = new WWWForm();
    //    UnityWebRequest www = UnityWebRequest.Get("http://134.209.103.120/Casino/mapi/1/login");


    //    www.SetRequestHeader("Authorization", Token_autorization);
    //    yield return www.SendWebRequest();
    //    if (www.isNetworkError || www.isHttpError)
    //    {
    //        Debug.Log(www.error);
    //    }
    //    else
    //    {
    //        rawJson = www.downloadHandler.text;
    //        Get_response_data();
    //        Debug.Log("Form upload complete!");
    //    }
    //}

    //public void get_Currency_XP()
    //{
    //    wallet_coin = long.Parse(PlayerPrefs.GetString("Slot_credits"));
    //    current_xp = PlayerPrefs.GetInt("CurrentXP", 0);
    //}

    //public void Convert_image_to_base64()
    //{


    //    bytes = mytexture.EncodeToPNG();
    //    string enc = Convert.ToBase64String(bytes);
    //    print(mytexture.name);
    //    StartCoroutine("Upload");
    //}



}