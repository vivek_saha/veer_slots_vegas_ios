﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Xp_coin_effect_script : MonoBehaviour
{

    public RectTransform xp_start, xp_end;
    public GameObject star_prefab, star_object, star_effect_obj;
    public Transform content;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void Add_xp()
    {
        GameObject temp = Instantiate(star_prefab, Vector3.zero, Quaternion.identity);
        temp.transform.SetParent(content);
        temp.transform.localScale = Vector3.one;
        temp.transform.position = xp_start.position;
        temp.GetComponent<RectTransform>().DOMove(xp_end.position, 01f).OnComplete(() => { Destroy(temp); star_size_changer(); });

    }
    public void star_size_changer()
    {
        star_object.transform.localScale = Vector3.one;
        star_object.transform.DOPunchScale(new Vector3(0.1f, 0.1f), 0.5f).OnComplete(() => star_effect());
    }

    public void star_effect()
    {
        star_effect_obj.SetActive(true);
        Invoke("stop_star_effect", 1f);
    }

    public void stop_star_effect()
    {
        star_effect_obj.SetActive(false);
    }
}
