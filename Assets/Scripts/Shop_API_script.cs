using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SimpleJSON;
using UnityEngine.Networking;
using IAP;

public class Shop_API_script : MonoBehaviour
{
    public static Shop_API_script Instance;
    string url = "http://134.209.103.120/Casino/mapi/1/coin-master/eyJ0eXAiOiJKV1QiLCdlcMdGciOiJSUzI1";
    public JSONNode jsonResult;

    // create the web request and download handler
    //UnityWebRequest webReq = new UnityWebRequest();

    string rawJson;
    // Start is called before the first frame update

    private void Awake()
    {
        Instance = this;

    }
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    public void Get_settings_data()
    {
        //Debug.Log("getdata");
        StartCoroutine("GetData");

    }

    IEnumerator GetData()
    {
        UnityWebRequest www = UnityWebRequest.Get(url);
        //req_call = false;
        www.SetRequestHeader("Authorization", PlayerPrefs.GetString("Token"));
        //token = PlayerPrefs.GetString("Token");
        yield return www.Send();
        //Debug.Log(www);
        if (www.isNetworkError)
        {
            Debug.Log(www.error);
            Get_settings_data();

            //Check_Internet_connection();
            //req_call = true;
        }
        else
        {
            //close_Retry_panel();
            // Show results as text
            //Debug.Log(www.downloadHandler.text);
            rawJson = www.downloadHandler.text;
            jsonResult = JSON.Parse(rawJson);
            Set_Json_data();
            //Manual_Start();
            // Or retrieve results as binary data
            // byte[] results = www.downloadHandler.data;
        }
    }
    public void Set_Json_data()
    {
#if UNITY_IOS
string ad ="ios_";
#else
        string ad = "";
#endif
        int m = 0;
        foreach (JSONNode message in jsonResult["data"][ad+"purchase_coin"])
        {
            m++;
        }

        //Debug.Log("m" + m);
        Get_API_Data_IAP.Instance.first_val = new long[m];
        Get_API_Data_IAP.Instance.percentage_val = new int[m];
        Get_API_Data_IAP.Instance.price_val = new string[m];
        Get_API_Data_IAP.Instance.product_id = new string[m];


        int counter = 0;
        foreach (JSONNode message in jsonResult["data"][ad+"purchase_coin"])
        {

            Get_API_Data_IAP.Instance.first_val[counter] = message["coin"].AsLong;
            Get_API_Data_IAP.Instance.percentage_val[counter] = message["inr"].AsInt;
            Get_API_Data_IAP.Instance.price_val[counter] = message["is_video"].Value;
            Get_API_Data_IAP.Instance.product_id[counter] = message["product_id"].Value;

            counter++;
        }

        foreach (JSONNode message in jsonResult["data"][ad + "offer_coin"])
        {
            Get_API_Data_IAP.Instance.St_purchase_first_val = message["coin"].AsLong;
            Get_API_Data_IAP.Instance.St_purchase_percentage_val = message["inr"].AsInt;
            Get_API_Data_IAP.Instance.St_purchase_price_val = message["is_video"].Value;
            Get_API_Data_IAP.Instance.St_purchase_product_id = message["product_id"].Value;
        }
        Gamemanager.Instance.gameObject.transform.GetComponent<InAppManager>().manual_start();
        //Gamemanager.Instance.Generate_shop_panel();
        //if(Get_API_Data_IAP.Instance.St_purchase_product_id!=null)
        //{
        //    Get_API_Data_IAP.Instance.startup_purchase_card();
        //}
    }

}
