﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using IAP;

public class Purchase_container_script : MonoBehaviour
{

    public TextMeshProUGUI Main_value;
    public TextMeshProUGUI Percentage_value;
    public TextMeshProUGUI Final_value;
    public Text Buy_value;
    public GameObject buy_button;

    public int number;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }


    public void ONCLICK_but()
    {
        buy_button.GetComponent<Button>().onClick.AddListener(() => onclick_fun());
    }
    public void onclick_fun()
    {
        Audio_manager.Instance.Simple_button_click();
        if (number == -1)
        {
            Gamemanager.Instance.gameObject.transform.GetComponent<InAppManager>().BuyProductID(Get_API_Data_IAP.Instance.St_purchase_product_id);
        }
        else
        {
            Gamemanager.Instance.gameObject.transform.GetComponent<InAppManager>().BuyProductID(Get_API_Data_IAP.Instance.product_id[number]);
        }
    }
}
