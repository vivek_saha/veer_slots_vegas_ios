﻿using System.Collections;
using UnityEngine;
using System;
using System.Net; //Most important Library
using System.IO;

public class DownloadClass : MonoBehaviour
{


    public static DownloadClass Instance;
    //Your URL path
    string url = "http://134.209.103.120/Casino/public/uploads/level/21.lvl";
    //Create a Client instance to download the WebRequest
    WebClient client = new WebClient();
    Uri uri;

    bool downloading = false;
    int L;


    int progress;
    private void Awake()
    {
        Instance = this;
    }
    private void Start()
    {


    }


    public void Start_download(int lvl)
    {
        L = lvl;
        Debug.Log("start_download");
        url= "http://134.209.103.120/Casino/public/uploads/level/ios/"+lvl+".lvl";
        //Create a WebRequest instance
        uri = new Uri(url);
        StartCoroutine("DownLoadAsset",lvl);
        downloading = true;
            client.DownloadProgressChanged += Client_DownloadProgressChanged;
    }

    private void Update()
    {
    }

    //Create your coroutine to download your AssetBundle or any file (mp3, jpeg, etc)
    public IEnumerator DownLoadAsset(int lvl)
    {
        //Start download and specify the save path
        client.DownloadFileAsync(uri, Application.persistentDataPath + "/Levels/"+lvl+".lvl");

        while (client.IsBusy)   // Wait until the file download is complete
            yield return null;

        if (downloading)
        {
            //Suscribe to the event ProgressChanged
        }
        //Now your file is completely downloaded and saved
    }

    //Create your ProgressChanged "Listener"
    private void Client_DownloadProgressChanged(object sender, DownloadProgressChangedEventArgs e)
    {
        //Show download progress
        //Debug.Log("Download Progress: " + e.ProgressPercentage);
        //Gamemanager.Instance.Set_download_fill_value(L,e.ProgressPercentage);
        progress = e.ProgressPercentage;
        GetComponent<Level_Selection_script>().Set_fill_amount((float)e.ProgressPercentage / 100f);
        if(e.ProgressPercentage>=100)
        {
            downloading = false;
            //Gamemanager.Instance.DOwnloadComplete(L);
            GetComponent<Level_Selection_script>().Download_Complete();
        }
    }


    private void OnApplicationQuit()
    {
        if(progress!=100)
        {
            if (!Directory.Exists(Application.persistentDataPath + "/Levels"))
            {
                //if it doesn't, create it
                //Directory.CreateDirectory(Application.persistentDataPath + "/Levels");

            }

            else
            {
                string filePath = Application.persistentDataPath + "/Levels/" + L + ".lvl";

                if (System.IO.File.Exists(filePath))
                {
                    // The file exists -> run event
                    File.Delete(filePath);
                }
                else
                {
                    // The file does not exist -> run event
                }
            }
        }
    }
}