﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Start_lederboard_panel : MonoBehaviour
{
    public CanvasGroup scrollview;
    public GameObject  Loading_panel;
    // Start is called before the first frame update
    public void Manual_Start()
    {
        scrollview.alpha = 0;
        Loading_panel.SetActive(true);
        StartCoroutine("Loading_wait");
    }

    
    IEnumerator Loading_wait()
    {
        yield return new WaitForSeconds(2f);
        Loading_panel.SetActive(false);
        scrollview.alpha = 1;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
